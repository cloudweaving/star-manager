// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

import { Form, Input, PageHeader } from 'antd';
import React, { useContext } from 'react';
import ProCard from '@ant-design/pro-card';
import { NewSpaceInfoContext } from '@src/common/common.context';
import styles from './index.module.less';

export function InstallOptions(props: any) {
    const { form } = useContext(NewSpaceInfoContext);
    return (
        <ProCard title={<h2>安装选项</h2>} headerBordered>
            <PageHeader className="site-page-header" title="获取starRocks镜像" style={{ paddingLeft: 0 }} />
            <p>
                <div>Star Manager将从提供的地址直接获取starRocks镜像。</div>
            </p>
            <Form form={form} name="basic" autoComplete="off">
                <Form.Item label="FE镜像地址" name="feImageUrl" rules={[{ required: true, message: '请输入FE镜像地址' }]}>
                    <Input className={styles.input} />
                </Form.Item>
                <Form.Item label="BE镜像地址" name="beImageUrl" rules={[{ required: true, message: '请输入BE镜像地址' }]}>
                    <Input className={styles.input} />
                </Form.Item>
            </Form>
            <PageHeader className="site-page-header" title="指定安装路径" style={{ paddingLeft: 0 }} />
            <div>
                <p>数据库组件与Engine Manager Agent将安装至该目录下。请确保该目录为数据库及相关组件专用。</p>
            </div>
            <Form form={form} name="basic" autoComplete="off">
                <Form.Item label="安装路径" name="installDir" rules={[{ required: true, message: '请输入安装路径' }]} initialValue={"/opt/starrocks"}>
                    <Input className={styles.input} />
                </Form.Item>
                <Form.Item label="数据路径" name="dataDir" rules={[{ required: true, message: '请输入数据存储路径' }]} initialValue={"/data/starrocks"}>
                    <Input className={styles.input} />
                </Form.Item>
                <Form.Item label="初始LEADER" name="leaderIP" rules={[{ required: true, message: '请输入初始LEADER节点IP地址' }]}>
                    <Input className={styles.input} />
                </Form.Item>
                <Form.Item
                    label="Agent启动端口"
                    name="agentPort"
                    rules={[{ required: true, message: '请输入Agent启动端口' }]}
                >
                    <Input className={styles.input} />
                </Form.Item>
            </Form>
        </ProCard>
    );
}
