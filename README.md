<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

# Star Manager

The repository contains Manager for StarRocks Engine.

## License

[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Report issues or submit pull request

If you find any bugs, feel free to file a [issue](https://gitee.com/cloudweaving/star-manager/issues) or fix it by submitting a [pull request](https://gitee.com/cloudweaving/star-manager/pulls).

## Contact Us

Contact us through the following mailing list.


## Links

*  Thanks for the Doris manager project - <https://github.com/apache/doris-manager>

# Star Manager 中文说明

该仓库包含StarRocks引擎的管理器。

## 许可证

[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## 报告问题或提交拉取请求

如果您发现任何错误，请通过[问题](https://gitee.com/cloudweaving/star-manager/issues)或提交[拉取请求](https://gitee.com/cloudweaving/star-manager/pulls)来修复它。

## 特别说明

目前基于doris-manager 1.0.0-beta 版本改造。

已经完成集群创建、集群托管相关内容改造，后续会继续完善。

与doris-manager 1.0.0-beta的特性对比：

* 集群创建：为避免兼容性问题，创建的集群是跑在docker中的，不同于官方手动部署方案和k8s部署方案，是物理机安装docker后，fe和be跑在docker中。此方案在centos 7.9、openEuler 20.03 SP3、openEuler 22.03 SP1系统测试通过。
* 集群托管：集群托管后，集群的所有监控接口都重新做了适配，集群概览、集群列表、参数配置都是通过JDBC的方式获取，监控信息是从Prometheus获取。
* manager运行环境：manager可以运行在虚拟机环境，或者k8s环境中，运行在k8s环境中时，需要额外维护manager.conf中的RUN_ENV_TYPE=KUBERNETES、INGRESS_PORT=8089、INGRESS_HOST=star-manager参数。

目前从代码层次，架构设计上都很粗糙，欢迎批评指正。

## 如何开始

编译参考：[Star Manager 编译部署文档](https://gitee.com/cloudweaving/star-manager/blob/master/docs/Star%20Manager%20Compiling%20&%20Deploying.md)

部署参考：[Star Manager 创建集群与接管集群](https://gitee.com/cloudweaving/star-manager/blob/master/docs/Star%20Manger%20Initializing.md)

## 下一步规划

下一步的规划：

* 通过实际应用，修复问题，改善易用性。
* 参数配置还没有打通，目前只能查看fe的参数，不能修改，不能查看be参数。
* conf文件在集群创建后，还不能修改，需要增加修改能力。
* 还有许多没有增加的能力，比如：集群扩容、集群缩容、集群删除、集群备份、集群恢复等。

以上是对Star Manager的简要说明，项目人力受限，欢迎各位大佬提出issue，或者直接提交pr。

