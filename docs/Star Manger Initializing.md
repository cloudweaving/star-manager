<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

# 初始化

完成部署之后，系统管理员需要完成本地初始化。

初始化分成两步，选择用户认证方式和新建空间连接集群。

## 管理用户

初始化第一步为管理用户，主要完成对认证方式的选择和配置。目前Star Manager支持本地用户认证。

![image.png](images/init-select.png)

### 本地用户认证

本地用户认证是Star Manager自带的用户系统。通过填入用户名、邮箱和密码可完成用户注册，用户的新增、信息修改、删除和权限关系均在本地内完成。

初始化时超级管理员选择后，无需配置任何信息，直接进行初始化的第二步，创建空间连接集群。

![image.png](images/init-config.png)

## 新建空间

新建空间包括新建集群和集群托管两种方式。

### 新建StarRocks集群

#### 1 注册空间

空间信息包括空间名称、空间简介、选择空间管理员。

空间名称、管理员为必填/选字段。

![image.png](images/create-cluster-1.png)

#### 2 添加主机

![image.png](images/create-cluster-1.png)

##### 容器化部署starrocks准备工作

基于Star Manager进行StarRocks部署实际上是基于docker容器化部署，因此需要完成以下准备工作：

(1) Star Manager管理节点安装mysql客户端 （基于docker版本的Star Manager无需安装mysql客户端）

(2) 配置Star Manager管理节点至agent节点 root 用户SSH免密 (免密步骤见文档最后一节)

(3) agent节点均需安装docker并配置好仓库凭证,由于镜像体积较大,建议部署前将fe及be的镜像pull至本地

(4) agent节点需配置主机名(如hostnamectl set-hostname sr01),并更新集群域名映射到/etc/hosts

StarRocks的FE与BE的镜像地址为(企业内部部署人员由于网络限制，不使用如下的地址，请向支持人员索取企业内部地址)：

```shell
registry.cn-qingdao.aliyuncs.com/cloudweaving/datafabric-starrocks-fe:2.5.13-24022701
registry.cn-qingdao.aliyuncs.com/cloudweaving/datafabric-starrocks-be:2.5.13-24022701
```

##### 主机列表

输入主机IP添加新的主机，也可通过批量添加。

#### 3 安装选项

![image.png](images/install-optition.png)

##### 获取starrocks镜像

###### 填写前面提到的FE与BE镜像地址

##### 指定安装路径

starrocks与Star Manager Agent将安装至该目录下。请确保该目录为starrocks以及相关组件专用。

#### 4 校验主机

系统会根据主机状态自动进行校验，当校验完成时既Agent启动回传心跳，可点击进行下一步。

![image.png](images/check-nodes.png)

#### 5 规划节点

点击分配节点按钮，对主机进行FE/BE/Broker节点的规划。

![image.png](images/plan-nodes.png)

#### 6 配置参数

对上一步规划的节点进行配置参数，可以使用默认值也可以打开自定义配置开关对配置进行自定义。

![image.png](images/config-cluster.png)

#### 7 部署集群

系统会根据主机安装进度状态自动进行校验，当校验完成时既启动节点并回传心跳，可点击进行下一步。

![image.png](images/deploy-cluster.png)

#### 8 完成创建

完成以上步骤即可完成新建集群。

![image.png](images/deploy-complete.png)

### 集群托管

#### 1 注册空间

空间信息包括空间名称、空间简介、选择空间管理员。

空间名称、管理员为必填/选字段。
![image.png](images/takeover-space.png)

#### 2 连接集群

集群信息包括集群地址、HTTP端口、JDBC端口、集群用户名和集群密码。用户可根据自身集群信息进行填写。

点击链接测试按钮进行测试。

![image.png](images/takeover-linkCluster.png)

#### 3 托管选项

![image.png](images/takeover-optition.png)

点击下一步之前需要进行免密设置，免密步骤见文档最后一节。

##### 指定安装路径

Star与Star Manager Agent将安装至该目录下。请确保该目录为Doirs以及相关组件专用。

#### 4 校验主机

系统会根据主机状态自动进行校验，当校验完成时既Agent启动回传心跳，可点击进行下一步。

![image.png](images/takeover-checknodes.png)

#### 5 校验集群

校验集群分位实例安装校验、实例依赖校验、实例启动校验，校验成功后点击下一步即可完成创建。

![image.png](images/takeover-check-cluster.png)

#### 6 完成接入

完成以上步骤即可完成集群托管。

![image.png](images/takeover-complete.png)

### 空间列表

点击左侧导航栏进入空间列表页，空间列表页用于展示用户拥有权限的空间名称。

![image.png](images/space-list.png)

空间列表分为已完成空间和未完成空间，已完成空间可以对其进行操作如进入空间、删除和编辑。
注意：仅空间管理员有权限进行删除和编辑权限。

进入空间后,可以看到更多集群信息:

![image.png](images/cluster-overview.png)

可以对空间名称、空间简介、空间管理员进行相应编辑:

![image.png](images/space-edit.png)

点击删除按钮后弹窗是否删除，点击确定删除成功。

![image.png](images/delete-space.png)

![image.png](images/space-deleted.png)

点击上方二级导航栏进入未完成空间列表，未完成空间列表记录上次创建空间但未完成的操作，空间管理员可以继续对未完成空间进行恢复或者删除操作。

![image.png](images/space-list-pre.png)

此时已经完成了本地初始化过程。空间管理员可以进入空间，进行空间管理，邀请用户进入空间进行数据分析等工作。

### 配置SSH免登陆

Star Manager 在安装时需要分发 Agent 安装包，故需要在待安装 StarRocks 的服务器 (agent01) 配置 SSH 免登陆。

独立虚拟机或者物理机部署 Star Manager时，遵循以下步骤：

```shell
#1.登录服务器，需要使用manager和agent账号保持一致
su - xxx
pwd
#2.在部署star manager机器上生成密钥对
ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa

#3.将公钥拷贝到机器agent01上
scp  ~/.ssh/id_rsa.pub root@agent01:~

#4.登录agent01，将公钥追加到authorized_keys 
cat ~/id_rsa.pub >> .ssh/authorized_keys

#5.这样做完之后我们就可以在Star Manager机器免密码登录agent01
ssh root@agent01
```

使用Docker版本的 Star Manager 时，遵循以下步骤：

```shell
#1.进入star manager镜像命令行，执行以下脚本，脚本中的root为star manager主机的root用户
docker exec -it starmanager /bin/bashbash
ssh-copy-id root@agent01

#2.这样做完之后我们就可以在Star Manager机器免密码登录agent01
ssh root@agent01
```

另外需要注意：

1）.ssh目录的权限为700，其下文件authorized_keys和私钥的权限为600。否则会因为权限问题导致无法免密码登录。我们可以看到登陆后会有known_hosts文件生成。同时启动StarRocks时需要使用免密码登录的账号。

2)托管集群时，之前在agent机器上部署starrocks时，也要使用ssh免密码登陆的同一个账号，否则会出现权限问题。

在Star Manager 安装集群时，使用部署star manager机器的私钥即可，即~/.ssh/id_rsa

详细可参考：https://blog.csdn.net/universe_hao/article/details/52296811