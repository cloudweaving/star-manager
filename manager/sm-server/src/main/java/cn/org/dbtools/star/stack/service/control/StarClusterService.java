// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.service.control;

import cn.org.dbtools.star.stack.component.ClusterUserComponent;
import cn.org.dbtools.star.stack.connector.StarMetaInfoClient;
import cn.org.dbtools.star.stack.control.ModelControlRequestType;
import cn.org.dbtools.star.stack.control.ModelControlResponse;
import cn.org.dbtools.star.stack.control.request.StarClusterRequest;
import cn.org.dbtools.star.stack.control.request.content.StarClusterCreationRequest;
import cn.org.dbtools.star.stack.control.request.content.StarClusterTakeOverRequest;
import cn.org.dbtools.star.stack.control.request.handler.StarClusterCreationRequestHandler;
import cn.org.dbtools.star.stack.control.request.handler.StarClusterStartRequestHandler;
import cn.org.dbtools.star.stack.control.request.handler.StarClusterTakeOverRequestHandler;
import cn.org.dbtools.star.stack.dao.ClusterInfoRepository;
import cn.org.dbtools.star.stack.dao.ClusterInstanceRepository;
import cn.org.dbtools.star.stack.dao.ClusterModuleRepository;
import cn.org.dbtools.star.stack.dao.ResourceNodeRepository;
import cn.org.dbtools.star.stack.driver.JdbcSampleClient;
import cn.org.dbtools.star.stack.entity.ClusterInfoEntity;
import cn.org.dbtools.star.stack.entity.ClusterInstanceEntity;
import cn.org.dbtools.star.stack.entity.ClusterModuleServiceEntity;
import cn.org.dbtools.star.stack.entity.HeartBeatEventEntity;
import cn.org.dbtools.star.stack.entity.ResourceNodeEntity;
import cn.org.dbtools.star.stack.model.request.control.DeployConfigItem;
import cn.org.dbtools.star.stack.model.response.control.ClusterInstanceInfo;
import cn.org.dbtools.star.stack.model.response.control.ClusterInstanceInfoExt;
import cn.org.dbtools.star.stack.model.response.control.ClusterModuleInfo;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import cn.org.dbtools.star.manager.common.util.ConfigDefault;
import cn.org.dbtools.star.manager.common.util.ServerAndAgentConstant;
import cn.org.dbtools.star.stack.connector.StarStatisticClient;
import cn.org.dbtools.star.stack.control.request.handler.StarClusterRestartRequestHandler;
import cn.org.dbtools.star.stack.control.request.handler.StarClusterStopRequestHandler;
import cn.org.dbtools.star.stack.dao.ClusterModuleServiceRepository;
import cn.org.dbtools.star.stack.dao.HeartBeatEventRepository;
import cn.org.dbtools.star.stack.entity.ClusterModuleEntity;
import cn.org.dbtools.star.stack.entity.CoreUserEntity;
import cn.org.dbtools.star.stack.model.request.control.StarClusterCreationReq;
import cn.org.dbtools.star.stack.model.request.control.StarClusterModuleDeployConfig;
import cn.org.dbtools.star.stack.model.request.control.StarClusterTakeOverReq;
import cn.org.dbtools.star.stack.model.response.control.ResourceNodeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class StarClusterService {

    @Autowired
    private ClusterInfoRepository clusterInfoRepository;

    @Autowired
    private HeartBeatEventRepository heartBeatEventRepository;

    @Autowired
    private ResourceNodeRepository nodeRepository;

    @Autowired
    private ClusterModuleRepository moduleRepository;

    @Autowired
    private ClusterInstanceRepository instanceRepository;

    @Autowired
    private ClusterModuleServiceRepository serviceRepository;

    @Autowired
    private StarClusterCreationRequestHandler creationRequestHandler;

    @Autowired
    private StarClusterTakeOverRequestHandler takeOverRequestHandler;

    @Autowired
    private StarClusterStopRequestHandler stopRequestHandler;

    @Autowired
    private StarClusterStartRequestHandler startRequestHandler;

    @Autowired
    private StarClusterRestartRequestHandler restartRequestHandler;

    @Autowired
    private JdbcSampleClient jdbcSampleClient;

    @Autowired
    private ClusterUserComponent userComponent;

    @Autowired
    private StarMetaInfoClient metaInfoClient;
    @Autowired
    private StarStatisticClient starStatisticClient;

    @Autowired
    private ClusterUserComponent clusterUserComponent;

    public ModelControlResponse creation(CoreUserEntity user, StarClusterCreationReq creationReq) throws Exception {
        log.info("Rquest info is {}", JSON.toJSON(creationReq));

        StarClusterCreationRequest request = new StarClusterCreationRequest();
        request.setType(ModelControlRequestType.CREATION);
        request.setReqInfo(creationReq);
        request.setClusterId(creationReq.getClusterId());
        request.setRequestId(creationReq.getRequestId());
        request.setEventType(creationReq.getEventType());

        ModelControlResponse response = creationRequestHandler.handleRequest(user, request);
        return response;
    }

    public ModelControlResponse takeOver(CoreUserEntity user, StarClusterTakeOverReq takeOverReq) throws Exception {
        log.info("Rquest info is {}", JSON.toJSON(takeOverReq));
        StarClusterTakeOverRequest request = new StarClusterTakeOverRequest();
        request.setType(ModelControlRequestType.TAKE_OVER);
        request.setReqInfo(takeOverReq);
        request.setClusterId(takeOverReq.getClusterId());
        request.setRequestId(takeOverReq.getRequestId());
        request.setEventType(takeOverReq.getEventType());

        ModelControlResponse response = takeOverRequestHandler.handleRequest(user, request);
        return response;
    }

    public ModelControlResponse stopCluster(CoreUserEntity user, long clusterId) throws Exception {
        userComponent.checkUserSpuerAdminOrClusterAdmin(user, clusterId);
        StarClusterRequest request = new StarClusterRequest();
        request.setType(ModelControlRequestType.STOP);
        request.setClusterId(clusterId);
        request.setRequestId(0);
        ModelControlResponse response = stopRequestHandler.handleRequest(user, request);
        return response;
    }

    public ModelControlResponse startCluster(CoreUserEntity user, long clusterId) throws Exception {
        userComponent.checkUserSpuerAdminOrClusterAdmin(user, clusterId);
        StarClusterRequest request = new StarClusterRequest();
        request.setType(ModelControlRequestType.START);
        request.setClusterId(clusterId);
        request.setRequestId(0);
        ModelControlResponse response = startRequestHandler.handleRequest(user, request);
        return response;
    }

    public ModelControlResponse restartCluster(CoreUserEntity user, long clusterId) throws Exception {
        userComponent.checkUserSpuerAdminOrClusterAdmin(user, clusterId);
        StarClusterRequest request = new StarClusterRequest();
        request.setType(ModelControlRequestType.RESTART);
        request.setClusterId(clusterId);
        request.setRequestId(0);
        ModelControlResponse response = restartRequestHandler.handleRequest(user, request);
        return response;
    }

    /**
     * TODO:Subsequent improvement
     *
     * @return
     */
    public List<ClusterModuleInfo> getClusterModules(CoreUserEntity user, long clusterId) throws Exception {
        userComponent.checkUserSpuerAdminOrClusterAdmin(user, clusterId);

        List<ClusterModuleEntity> moduleEntities = moduleRepository.getByClusterId(clusterId);

        ClusterInfoEntity cluster = clusterInfoRepository.findById(clusterId).get();

        List<ClusterModuleInfo> moduleInfos = new ArrayList<>();
        for (ClusterModuleEntity moduleEntity : moduleEntities) {
            ClusterModuleInfo moduleInfo = new ClusterModuleInfo();
            moduleInfo.setModuleId(moduleEntity.getId());
            moduleInfo.setName(moduleEntity.getModuleName());

            // If it has been configured, the configuration information is used.
            // If it has not been configured, the default configuration information is used
            String configInfo = moduleEntity.getConfig();
            if (configInfo == null || configInfo.isEmpty()) {
                moduleInfo.setConfig(getModuleDefaultConfig(moduleEntity.getModuleName(), cluster.getDataDirInfo()));
            } else {
                StarClusterModuleDeployConfig config = JSON.parseObject(configInfo, StarClusterModuleDeployConfig.class);
                moduleInfo.setConfig(config.getConfigs());
            }
            moduleInfos.add(moduleInfo);

        }
        return moduleInfos;
    }

    /**
     * TODO:Subsequent improvement
     *
     * @return
     */
    public List<ClusterInstanceInfo> getClusterInstances(CoreUserEntity user, long clusterId) throws Exception {
        userComponent.checkUserSpuerAdminOrClusterAdmin(user, clusterId);
        ClusterInfoEntity clusterInfo = clusterInfoRepository.getById(clusterId);

        List<ClusterModuleEntity> moduleEntities = moduleRepository.getByClusterId(clusterId);

        List<String> activeFelist = new ArrayList<>();
        List<String> activeBelist = new ArrayList<>();
        if (clusterInfo.getAddress() != null && !clusterInfo.getAddress().isEmpty()) {
            activeFelist = metaInfoClient.getActiveFeList(clusterInfo);
            activeBelist = metaInfoClient.getActiveBeList(clusterInfo);
        }

        List<ClusterInstanceInfo> instanceInfos = new ArrayList<>();

        for (ClusterModuleEntity moduleEntity : moduleEntities) {
            List<ClusterInstanceEntity> instanceEntities = instanceRepository.getByModuleId(moduleEntity.getId());
            for (ClusterInstanceEntity instanceEntity : instanceEntities) {
                ClusterInstanceInfoExt instanceInfo = new ClusterInstanceInfoExt();
                instanceInfo.setModuleName(moduleEntity.getModuleName());
                instanceInfo.setInstanceId(instanceEntity.getId());
                instanceInfo.setNodeHost(instanceEntity.getAddress());
                if (moduleEntity.getModuleName().equalsIgnoreCase(ServerAndAgentConstant.FE_NAME)) {
                    instanceInfo.setDetailsLink(String.format("http://%s:8030", instanceEntity.getAddress()));
                } else if (moduleEntity.getModuleName().equalsIgnoreCase(ServerAndAgentConstant.BE_NAME)) {
                    instanceInfo.setDetailsLink(String.format("http://%s:8040", instanceEntity.getAddress()));
                }
                long eventId = instanceEntity.getCurrentEventId();
                String eventType = "";
                if (eventId > 0) {
                    HeartBeatEventEntity eventEntity = heartBeatEventRepository.findById(eventId).get();

                    instanceInfo.setOperateStatus(eventEntity.getStatus());
                    instanceInfo.setOperateResult(eventEntity.getOperateResult());
                    instanceInfo.setOperateStage(eventEntity.getStage());

                    eventType = eventEntity.getType();
                }

                // 检查节点状态 几种特殊状态：点击停止按钮后，刷新页面，BE停止中，停止任务执行中，此时状态为停止
                List<String> activelist = new ArrayList<>();
                if (instanceInfo.getModuleName().equalsIgnoreCase(ServerAndAgentConstant.FE_NAME)) {
                    activelist = activeFelist;
                } else if (instanceInfo.getModuleName().equalsIgnoreCase(ServerAndAgentConstant.BE_NAME)) {
                    activelist = activeBelist;
                }

                if (activelist.contains(instanceInfo.getNodeHost())
                        && instanceInfo.getOperateStatus().equalsIgnoreCase("SUCCESS")) {
                    if (eventType.equalsIgnoreCase("INSTANCE_START")
                            || eventType.equalsIgnoreCase("INSTANCE_DEPLOY_CHECK")
                            || eventType.equalsIgnoreCase("INSTANCE_INSTALL")) {
                        instanceInfo.setNodeStatus("SUCCESS");
                    } else if (eventType.equalsIgnoreCase("INSTANCE_STOP")) {
                        instanceInfo.setNodeStatus("FAIL");
                    }
                } else if (instanceInfo.getOperateStatus().equalsIgnoreCase("SUCCESS")
                        && eventType.equalsIgnoreCase("INSTANCE_STOP")) {
                    instanceInfo.setNodeStatus("FAIL");
                } else {
                    instanceInfo.setNodeStatus("PROCESSING");
                }

                instanceInfos.add(instanceInfo);
            }
        }
        return instanceInfos;
    }

    /**
     * TODO:Subsequent improvement
     *
     * @return
     */
    public List<ResourceNodeInfo> getClusterResourceNodes(CoreUserEntity user, long clusterId) throws Exception {
        userComponent.checkUserSpuerAdminOrClusterAdmin(user, clusterId);

        ClusterInfoEntity clusterInfo = clusterInfoRepository.findById(clusterId).get();
        long resourceClusterId = clusterInfo.getResourceClusterId();

        List<ResourceNodeEntity> nodeEntities = nodeRepository.getByResourceClusterId(resourceClusterId);

        List<ResourceNodeInfo> nodeInfos = new ArrayList<>();
        for (ResourceNodeEntity nodeEntity : nodeEntities) {
            ResourceNodeInfo nodeInfo = new ResourceNodeInfo();
            nodeInfo.setNodeId(nodeEntity.getId());
            nodeInfo.setHost(nodeEntity.getHost());

            long eventId = nodeEntity.getCurrentEventId();
            if (eventId > 0) {
                HeartBeatEventEntity eventEntity = heartBeatEventRepository.findById(eventId).get();

                nodeInfo.setOperateStatus(eventEntity.getStatus());
                nodeInfo.setOperateStage(eventEntity.getStage());
                nodeInfo.setOperateResult(eventEntity.getOperateResult());
            }

            nodeInfos.add(nodeInfo);
        }
        return nodeInfos;
    }

    public boolean checkJdbcServiceReady(CoreUserEntity user, long clusterId) throws Exception {
        userComponent.checkUserSpuerAdminOrClusterAdmin(user, clusterId);
        ClusterInfoEntity clusterInfoEntity = clusterInfoRepository.findById(clusterId).get();
        if (clusterInfoEntity.getAddress() != null && !clusterInfoEntity.getAddress().isEmpty()) {
            log.info("The cluster {} is already access user space.", clusterId);
            return true;
        }

        // 通过api检查，如果fe的api正常，等同于jdbc接口活着
        List<ClusterModuleServiceEntity> httpService =
                serviceRepository.getByClusterIdAndName(clusterId, ServerAndAgentConstant.FE_HTTP_SERVICE);
        if (httpService.isEmpty()) {
            log.warn("The cluster {} no have http service", clusterId);
            return false;
        }

        ClusterModuleServiceEntity httpServiceEntity = httpService.get(0);
        List<String> httpAccessInfo = JSON.parseArray(httpServiceEntity.getAddressInfo(), String.class);
        int feHttpPort = httpServiceEntity.getPort();

        return starStatisticClient.testFeHealthyWithHttpApi(httpAccessInfo.get(0), feHttpPort,
                ServerAndAgentConstant.USER_ROOT, null);

//        List<ClusterModuleServiceEntity> jdbcService =
//                serviceRepository.getByClusterIdAndName(clusterId, ServerAndAgentConstant.FE_JDBC_SERVICE);
//        if (jdbcService.isEmpty()) {
//            log.warn("The cluster {} no have jdbc service", clusterId);
//            return false;
//        }
//
//        ClusterModuleServiceEntity serviceEntity = jdbcService.get(0);
//        List<String> accessInfo = JSON.parseArray(serviceEntity.getAddressInfo(), String.class);
//        int feJdbcPort = serviceEntity.getPort();
//
//        return jdbcSampleClient.testConnetion(accessInfo.get(0), feJdbcPort, ServerAndAgentConstant.USER_ROOT, "");
    }

    private List<DeployConfigItem> getModuleDefaultConfig(String moduleName, String dataDirInfo) {
        // get default config
        // TODO: Later, it is implemented in module template
        List<DeployConfigItem> configItems = new ArrayList<>();
        if (moduleName.equals(ServerAndAgentConstant.BE_NAME)) {
            for (String key : ConfigDefault.BE_CONFIG_DEDAULT.keySet()) {
                DeployConfigItem configItem = new DeployConfigItem();
                String value = ConfigDefault.BE_CONFIG_DEDAULT.get(key);
                if (key.equals(ConfigDefault.BE_LOG_CONFIG_NAME) || key.equals(ConfigDefault.BE_DATA_CONFIG_NAME)) {
                    value = dataDirInfo + "/" + ServerAndAgentConstant.BE_NAME + value;
                }
                configItem.setKey(key);
                configItem.setValue(value);

                configItems.add(configItem);
            }
        } else if (moduleName.equals(ServerAndAgentConstant.FE_NAME)) {
            for (String key : ConfigDefault.FE_CONFIG_DEDAULT.keySet()) {
                DeployConfigItem configItem = new DeployConfigItem();
                String value = ConfigDefault.FE_CONFIG_DEDAULT.get(key);
                if (key.equals(ConfigDefault.FE_LOG_CONFIG_NAME) || key.equals(ConfigDefault.FE_META_CONFIG_NAME)) {
                    value = dataDirInfo + "/" + ServerAndAgentConstant.FE_NAME + value;
                }
                configItem.setKey(key);
                configItem.setValue(value);

                configItems.add(configItem);
            }
        } else {
            for (String key : ConfigDefault.BROKER_CONFIG_DEDAULT.keySet()) {
                DeployConfigItem configItem = new DeployConfigItem();
                String value = ConfigDefault.BROKER_CONFIG_DEDAULT.get(key);
                configItem.setKey(key);
                configItem.setValue(value);

                configItems.add(configItem);
            }
        }
        return configItems;
    }

}
