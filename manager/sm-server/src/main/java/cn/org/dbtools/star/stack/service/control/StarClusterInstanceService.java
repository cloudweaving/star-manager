// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.service.control;

import cn.org.dbtools.star.stack.component.ClusterUserComponent;
import cn.org.dbtools.star.stack.control.ModelControlRequestType;
import cn.org.dbtools.star.stack.control.ModelControlResponse;
import cn.org.dbtools.star.stack.control.manager.StarClusterInstanceManager;
import cn.org.dbtools.star.stack.control.request.StarInstanceRequest;
import cn.org.dbtools.star.stack.control.request.handler.StarInstanceRestartRequestHandler;
import cn.org.dbtools.star.stack.control.request.handler.StarInstanceStartRequestHandler;
import cn.org.dbtools.star.stack.control.request.handler.StarInstanceStopRequestHandler;
import cn.org.dbtools.star.stack.dao.ClusterInstanceRepository;
import cn.org.dbtools.star.stack.dao.ClusterModuleRepository;
import cn.org.dbtools.star.stack.entity.ClusterInstanceEntity;
import cn.org.dbtools.star.stack.entity.ClusterModuleEntity;
import cn.org.dbtools.star.stack.model.request.control.DeployConfigItem;
import cn.org.dbtools.star.stack.model.request.control.StarClusterModuleDeployConfig;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import cn.org.dbtools.star.manager.common.heartbeat.HeartBeatEventType;
import cn.org.dbtools.star.manager.common.heartbeat.config.InstanceInstallEventConfigInfo;
import cn.org.dbtools.star.stack.entity.CoreUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class StarClusterInstanceService {
    @Autowired
    private ClusterModuleRepository moduleRepository;

    @Autowired
    private ClusterInstanceRepository instanceRepository;

    @Autowired
    private StarClusterInstanceManager instanceManager;

    @Autowired
    private ClusterUserComponent userComponent;

    @Autowired
    private StarInstanceStopRequestHandler stopRequestHandler;

    @Autowired
    private StarInstanceStartRequestHandler startRequestHandler;

    @Autowired
    private StarInstanceRestartRequestHandler restartRequestHandler;

    public void operateInstance(CoreUserEntity user, long setInstanceId, String operateType) throws Exception {

        HeartBeatEventType eventType = HeartBeatEventType.valueOf(operateType);

        // TODO:Currently, only agent install is implemented

        ClusterInstanceEntity instanceEntity = instanceRepository.findById(setInstanceId).get();
        userComponent.checkUserSpuerAdminOrClusterAdmin(user, instanceEntity.getClusterId());
        ClusterModuleEntity module = moduleRepository.findById(instanceEntity.getModuleId()).get();

        if (eventType == HeartBeatEventType.INSTANCE_INSTALL) {
            StarClusterModuleDeployConfig deployConfig = JSON.parseObject(module.getConfig(),
                    StarClusterModuleDeployConfig.class);

            InstanceInstallEventConfigInfo configInfo = new InstanceInstallEventConfigInfo();
            configInfo.setModuleName(module.getModuleName());
            List<DeployConfigItem> configItems = deployConfig.getConfigs();
            for (DeployConfigItem configItem : configItems) {
                configInfo.addParm(configItem.getKey(), configItem.getValue());
            }
            configInfo.setPackageDir(deployConfig.getPackageDir());
            configInfo.setInstallInfo(instanceEntity.getInstallInfo());

            // TODO:Do not put the request ID temporarily
            instanceManager.deployOperation(instanceEntity, configInfo, 0L);
        } else if (eventType == HeartBeatEventType.INSTANCE_DEPLOY_CHECK) {
            // TODO:
            return;
        } else {
            throw new Exception("The instance operate type not support");
        }

    }

 public ModelControlResponse stopInstance(CoreUserEntity user, long instanceId) throws Exception {
        userComponent.checkUserSpuerAdminOrClusterAdmin(user, instanceId);
        StarInstanceRequest request = new StarInstanceRequest();
        request.setType(ModelControlRequestType.STOP);
        request.setInstanceId(instanceId);
        request.setRequestId(0);
        ModelControlResponse response = stopRequestHandler.handleRequest(user, request);
        return response;
    }

    public ModelControlResponse startInstance(CoreUserEntity user, long instanceId) throws Exception {
        userComponent.checkUserSpuerAdminOrClusterAdmin(user, instanceId);
        StarInstanceRequest request = new StarInstanceRequest();
        request.setType(ModelControlRequestType.START);
        request.setInstanceId(instanceId);
        request.setRequestId(0);
        ModelControlResponse response = startRequestHandler.handleRequest(user, request);
        return response;
    }

    public ModelControlResponse restartInstance(CoreUserEntity user, long instanceId) throws Exception {
        userComponent.checkUserSpuerAdminOrClusterAdmin(user, instanceId);
        StarInstanceRequest request = new StarInstanceRequest();
        request.setType(ModelControlRequestType.RESTART);
        request.setInstanceId(instanceId);
        request.setRequestId(0);
        ModelControlResponse response = restartRequestHandler.handleRequest(user, request);
        return response;
    }

}
