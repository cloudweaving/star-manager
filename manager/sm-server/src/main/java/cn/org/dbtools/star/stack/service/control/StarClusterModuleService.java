// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.service.control;

import cn.org.dbtools.star.stack.component.ClusterUserComponent;
import cn.org.dbtools.star.stack.dao.ClusterInfoRepository;
import cn.org.dbtools.star.stack.dao.ClusterInstanceRepository;
import cn.org.dbtools.star.stack.dao.ClusterModuleRepository;
import cn.org.dbtools.star.stack.dao.ResourceNodeRepository;
import cn.org.dbtools.star.stack.entity.ClusterInstanceEntity;
import cn.org.dbtools.star.stack.entity.ClusterModuleEntity;
import cn.org.dbtools.star.stack.entity.HeartBeatEventEntity;
import cn.org.dbtools.star.stack.entity.ResourceNodeEntity;
import cn.org.dbtools.star.stack.model.response.control.ClusterInstanceInfo;
import lombok.extern.slf4j.Slf4j;
import cn.org.dbtools.star.stack.dao.HeartBeatEventRepository;
import cn.org.dbtools.star.stack.entity.CoreUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class StarClusterModuleService {
    @Autowired
    private ResourceNodeRepository nodeRepository;

    @Autowired
    private ClusterInstanceRepository instanceRepository;

    @Autowired
    private HeartBeatEventRepository heartBeatEventRepository;

    @Autowired
    private ClusterModuleRepository moduleRepository;

    @Autowired
    private ClusterInfoRepository clusterInfoRepository;

    @Autowired
    private ClusterUserComponent userComponent;

    /**
     * TODO:Subsequent improvement
     * @return
     */
    public List<ClusterInstanceInfo> getClusterMoudleInstanceList(CoreUserEntity user, long moduleId) throws Exception {
        ClusterModuleEntity moduleEntity = moduleRepository.findById(moduleId).get();

        userComponent.checkUserSpuerAdminOrClusterAdmin(user, moduleEntity.getClusterId());

        List<ClusterInstanceEntity> instanceEntities = instanceRepository.getByModuleId(moduleId);

        List<ClusterInstanceInfo> instanceInfos = new ArrayList<>();
        for (ClusterInstanceEntity instanceEntity : instanceEntities) {
            ClusterInstanceInfo instanceInfo = new ClusterInstanceInfo();
            instanceInfo.setModuleName(moduleEntity.getModuleName());
            instanceInfo.setInstanceId(instanceEntity.getId());

            long eventId = instanceEntity.getCurrentEventId();
            HeartBeatEventEntity eventEntity = heartBeatEventRepository.findById(eventId).get();

            instanceInfo.setOperateStatus(eventEntity.getStatus());
            instanceInfo.setOperateResult(eventEntity.getOperateResult());
            instanceInfo.setOperateStage(eventEntity.getStage());

            ResourceNodeEntity nodeEntity = nodeRepository.findById(instanceEntity.getNodeId()).get();
            instanceInfo.setNodeHost(nodeEntity.getHost());

            instanceInfos.add(instanceInfo);
        }
        return instanceInfos;
    }

}
