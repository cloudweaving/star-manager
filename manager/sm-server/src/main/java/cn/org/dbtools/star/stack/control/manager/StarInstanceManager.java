// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.control.manager;

import cn.org.dbtools.star.stack.component.StarManagerUserSpaceComponent;
import cn.org.dbtools.star.stack.dao.ClusterInfoRepository;
import cn.org.dbtools.star.stack.dao.ClusterInstanceRepository;
import cn.org.dbtools.star.stack.dao.ClusterModuleRepository;
import cn.org.dbtools.star.stack.dao.ResourceClusterRepository;
import cn.org.dbtools.star.stack.driver.JdbcSampleClient;
import cn.org.dbtools.star.stack.entity.ClusterInstanceEntity;
import lombok.extern.slf4j.Slf4j;
import cn.org.dbtools.star.stack.dao.ClusterModuleServiceRepository;
import cn.org.dbtools.star.stack.entity.ClusterModuleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class StarInstanceManager {

    @Autowired
    private StarManagerUserSpaceComponent userSpaceComponent;

    @Autowired
    private ClusterInfoRepository clusterRepository;

    @Autowired
    private ClusterInstanceRepository instanceRepository;

    @Autowired
    private ClusterModuleRepository moduleRepository;

    @Autowired
    private ClusterModuleServiceRepository serviceRepository;

    @Autowired
    private ResourceClusterRepository resourceClusterRepository;

    @Autowired
    private ResourceClusterManager resourceClusterManager;

    @Autowired
    private StarInstanceModuleManager instanceClusterModuleManager;

    @Autowired
    private JdbcSampleClient jdbcClient;

    public void stopClusterOperation(long instanceId, long requestId) throws Exception {
        log.info("Stop instance {} operation.", instanceId);
        ClusterInstanceEntity instanceEntity = instanceRepository.getById(instanceId);
        ClusterModuleEntity moduleEntity = moduleRepository.getById(instanceEntity.getModuleId());
        instanceClusterModuleManager.stopOperation(moduleEntity, instanceEntity, requestId);
    }

    public void startClusterOperation(long instanceId, long requestId) throws Exception {
        log.info("Start instance {} operation.", instanceId);
        ClusterInstanceEntity instanceEntity = instanceRepository.getById(instanceId);
        ClusterModuleEntity moduleEntity = moduleRepository.getById(instanceEntity.getModuleId());
        instanceClusterModuleManager.startOperation(moduleEntity, instanceEntity, requestId);

    }

    public void reStartClusterOperation(long instanceId, long requestId) throws Exception {
        log.info("Restart instance {} operation.", instanceId);
        ClusterInstanceEntity instanceEntity = instanceRepository.getById(instanceId);
        ClusterModuleEntity moduleEntity = moduleRepository.getById(instanceEntity.getModuleId());
        instanceClusterModuleManager.restartOperation(moduleEntity, instanceEntity, requestId);

    }

}
