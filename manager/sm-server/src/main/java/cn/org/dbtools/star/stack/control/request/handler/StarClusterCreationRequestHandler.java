// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.control.request.handler;

import cn.org.dbtools.star.stack.dao.ClusterInfoRepository;
import cn.org.dbtools.star.stack.entity.ClusterInfoEntity;
import cn.org.dbtools.star.stack.model.request.space.ClusterCreateReq;
import lombok.extern.slf4j.Slf4j;
import cn.org.dbtools.star.stack.control.ModelControlResponse;
import cn.org.dbtools.star.stack.control.manager.StarClusterManager;
import cn.org.dbtools.star.stack.control.manager.ResourceClusterManager;
import cn.org.dbtools.star.stack.control.request.StarClusterRequest;
import cn.org.dbtools.star.stack.control.request.StarClusterRequestHandler;
import cn.org.dbtools.star.stack.control.request.content.StarClusterCreationRequest;
import cn.org.dbtools.star.stack.entity.CoreUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// TODO: Subsequent improvement of request content judgment and exception handling
@Slf4j
@Component
public class StarClusterCreationRequestHandler extends StarClusterRequestHandler {

    @Autowired
    private StarClusterManager starClusterManager;

    @Autowired
    private ResourceClusterManager resourceClusterManager;

    @Autowired
    private ClusterInfoRepository clusterInfoRepository;

    @Override
    public ModelControlResponse handleRequestEvent(CoreUserEntity user, StarClusterRequest creationRequest, boolean modelInit) throws Exception {
        // TODO:How to define enumeration constants
        StarClusterCreationRequest request = (StarClusterCreationRequest) creationRequest;
        switch (request.getEventType()) {
            case 1: // CREATE_CLUSTER_SPACE
                return handleCreateClusterSpaceEvent(user, request, modelInit);
            case 2: // CREATE_RESOURCE_CLUSTER
                return handleCreateResourceClusterEvent(user, request);
            case 3: // CONFIG_AND_START_RESOURCE_CLUSTER
                return handleConfigAndStartResourceClusterEvent(user, request);
            case 4: // RESOURCE_CLUSTER_STARTED
                return handleResourceClusterStartedEvent(user, request);
            case 5: // SCHEDULE_STAR_CLUSTER
                return handleScheduleStarClusterEvent(user, request);
            case 6: // CONFIG_AND_DEPLOY_STAR_CLUSTER
                return handleConfigAndDeployStarClusterEvent(user, request);
            case 7: // STAR_CLUSTER_DEPLOYED
                return handleStarClusterDeployedEvent(user, request);
            case 8: // ACCESS_STAR_CLUSTER
                return handleAccessStarClusterEvent(user, request);
            default:
                log.error("Event type error.");
                throw new Exception("Event type error.");
        }
    }

    @Override
    public long initRequestModel(StarClusterRequest request, String creator) throws Exception {
        StarClusterCreationRequest creationRequest = (StarClusterCreationRequest) request;
        return starClusterManager.initOperation(creationRequest.getReqInfo().getSpaceInfo(), creator);
    }

    // CREATE_CLUSTER_SPACE
    private ModelControlResponse handleCreateClusterSpaceEvent(CoreUserEntity user,
                                                               StarClusterCreationRequest request,
                                                               boolean isInit) throws Exception {
        long clusterId = request.getClusterId();
        if (!isInit) {
            starClusterManager.updateClusterOperation(user, clusterId,
                    request.getReqInfo().getSpaceInfo());
        }

        return getResponse(request, false);
    }

    // CREATE_RESOURCE_CLUSTER
    private ModelControlResponse handleCreateResourceClusterEvent(CoreUserEntity user,
                                                                  StarClusterCreationRequest request) {
        long clusterId = request.getClusterId();
        ClusterInfoEntity clusterInfoEntity = clusterInfoRepository.findById(clusterId).get();
        starClusterManager.createClusterResourceOperation(user, clusterInfoEntity, request.getReqInfo().getAuthInfo(),
                request.getReqInfo().getHosts());

        return getResponse(request, false);
    }

    // CONFIG_AND_START_RESOURCE_CLUSTER
    private ModelControlResponse handleConfigAndStartResourceClusterEvent(CoreUserEntity user,
                                                                          StarClusterCreationRequest request)
            throws Exception {
        long clusterId = request.getClusterId();
        ClusterInfoEntity clusterInfoEntity = clusterInfoRepository.findById(clusterId).get();
        starClusterManager.configClusterResourceOperation(clusterInfoEntity, request.getReqInfo().getFeImageInfo(), request.getReqInfo().getBeImageInfo(),
                request.getReqInfo().getInstallInfo(), request.getReqInfo().getDataDirInfo(), request.getReqInfo().getAgentPort());
        starClusterManager.startClusterResourceOperation(clusterInfoEntity, request.getRequestId());

        return getResponse(request, false);
    }

    // RESOURCE_CLUSTER_STARTED
    private ModelControlResponse handleResourceClusterStartedEvent(CoreUserEntity user,
                                                                   StarClusterCreationRequest request) throws Exception {
        long clusterId = request.getClusterId();
        ClusterInfoEntity clusterInfoEntity = clusterInfoRepository.findById(clusterId).get();
        resourceClusterManager.checkNodesAgentOperation(clusterInfoEntity.getResourceClusterId());
        return getResponse(request, false);
    }

    // SCHEDULE_STAR_CLUSTER
    private ModelControlResponse handleScheduleStarClusterEvent(CoreUserEntity user,
                                                                 StarClusterCreationRequest request) throws Exception {
        long clusterId = request.getClusterId();

        starClusterManager.scheduleClusterOperation(clusterId, request.getReqInfo().getNodeConfig());

        return getResponse(request, false);
    }

    // CONFIG_AND_DEPLOY_STAR_CLUSTER
    private ModelControlResponse handleConfigAndDeployStarClusterEvent(CoreUserEntity user,
                                                                        StarClusterCreationRequest request) {
        long clusterId = request.getClusterId();

        ClusterInfoEntity clusterInfoEntity = clusterInfoRepository.findById(clusterId).get();
        starClusterManager.configClusterOperation(clusterInfoEntity, request.getReqInfo());
        starClusterManager.deployClusterOperation(clusterId, request.getRequestId());

        return getResponse(request, false);
    }

    // STAR_CLUSTER_DEPLOYED
    private ModelControlResponse handleStarClusterDeployedEvent(CoreUserEntity user,
                                                                 StarClusterCreationRequest request) throws Exception {
        long clusterId = request.getClusterId();
        starClusterManager.checkClusterInstancesOperation(clusterId);
        return getResponse(request, false);
    }

    // ACCESS_STAR_CLUSTER
    private ModelControlResponse handleAccessStarClusterEvent(CoreUserEntity user,
                                                               StarClusterCreationRequest request) throws Exception {
        long clusterId = request.getClusterId();

        String newPassword = request.getReqInfo().getClusterPassword();
        ClusterCreateReq clusterAccessInfo = starClusterManager.deployClusterAfterOperation(clusterId, newPassword);

        starClusterManager.clusterAccessOperation(clusterId, clusterAccessInfo);

        return getResponse(request, true);
    }

}
