// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.control.manager;

import cn.org.dbtools.star.stack.dao.ClusterInstanceRepository;
import cn.org.dbtools.star.stack.dao.ClusterModuleRepository;
import cn.org.dbtools.star.stack.entity.ClusterInstanceEntity;
import lombok.extern.slf4j.Slf4j;
import cn.org.dbtools.star.manager.common.heartbeat.config.InstanceRestartEventConfigInfo;
import cn.org.dbtools.star.manager.common.heartbeat.config.InstanceStartEventConfigInfo;
import cn.org.dbtools.star.manager.common.heartbeat.config.InstanceStopEventConfigInfo;
import cn.org.dbtools.star.stack.dao.ClusterModuleServiceRepository;
import cn.org.dbtools.star.stack.entity.ClusterModuleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class StarInstanceModuleManager {
    @Autowired
    private ClusterModuleRepository clusterModuleRepository;

    @Autowired
    private ClusterInstanceRepository instanceRepository;

    @Autowired
    private ClusterModuleServiceRepository serviceRepository;

    @Autowired
    private StarClusterInstanceManager instanceManager;

    public void stopOperation(ClusterModuleEntity module, ClusterInstanceEntity instanceEntity, long requestId) {
        log.info("stop instance {} for request {}", instanceEntity.getId(), requestId);

        InstanceStopEventConfigInfo configInfo = new InstanceStopEventConfigInfo();
        configInfo.setModuleName(module.getModuleName());

        configInfo.setInstallInfo(instanceEntity.getInstallInfo());
        instanceManager.stopOperation(instanceEntity, configInfo, requestId);
    }

    public void startOperation(ClusterModuleEntity module, ClusterInstanceEntity instanceEntity, long requestId) {
        log.info("start instance {} for request {}", instanceEntity.getId(), requestId);

        InstanceStartEventConfigInfo configInfo = new InstanceStartEventConfigInfo();
        configInfo.setModuleName(module.getModuleName());

        configInfo.setInstallInfo(instanceEntity.getInstallInfo());
        instanceManager.startOperation(instanceEntity, configInfo, requestId);
    }

    public void restartOperation(ClusterModuleEntity module, ClusterInstanceEntity instanceEntity, long requestId) {
        log.info("restart instance {} for request {}", instanceEntity.getId(), requestId);

        InstanceRestartEventConfigInfo configInfo = new InstanceRestartEventConfigInfo();
        configInfo.setModuleName(module.getModuleName());

        configInfo.setInstallInfo(instanceEntity.getInstallInfo());
        instanceManager.restartOperation(instanceEntity, configInfo, requestId);
    }

}
