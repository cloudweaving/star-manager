// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.model.request.control;

import cn.org.dbtools.star.stack.model.request.space.ClusterCreateReq;
import cn.org.dbtools.star.stack.model.request.space.NewUserSpaceCreateReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StarClusterTakeOverReq extends ModelControlReq {

    // Step 1: create space information
    private NewUserSpaceCreateReq spaceInfo;

    // Step 2: access star cluster
    private ClusterCreateReq clusterAccessInfo;

    // Step 3: create and config resource cluster and install agent
    private PMResourceClusterAccessInfo authInfo;

    private String installInfo;

    private String dataDirInfo;

    private int agentPort;

    // Step 4: check Install agent
    // Step 5: create cluster module and instance, check agent instance
}
