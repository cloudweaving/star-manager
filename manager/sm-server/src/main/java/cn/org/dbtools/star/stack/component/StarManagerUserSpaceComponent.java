// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.component;

import cn.org.dbtools.star.stack.control.ModelControlLevel;
import cn.org.dbtools.star.stack.control.ModelControlRequestType;
import cn.org.dbtools.star.stack.dao.ClusterInfoRepository;
import cn.org.dbtools.star.stack.dao.ModelControlRequestRepository;
import cn.org.dbtools.star.stack.entity.ClusterInfoEntity;
import cn.org.dbtools.star.stack.entity.ModelControlRequestEntity;
import cn.org.dbtools.star.stack.exception.StarJdbcPortErrorException;
import cn.org.dbtools.star.stack.exception.NameDuplicatedException;
import cn.org.dbtools.star.stack.exception.RequestFieldNullException;
import cn.org.dbtools.star.stack.exception.StudioNotInitException;
import cn.org.dbtools.star.stack.exception.UserNotExistException;
import cn.org.dbtools.star.stack.model.request.space.ClusterCreateReq;
import cn.org.dbtools.star.stack.model.request.space.NewUserSpaceCreateReq;
import cn.org.dbtools.star.stack.model.response.space.NewUserSpaceInfo;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import cn.org.dbtools.star.stack.connector.StarLoginClient;
import cn.org.dbtools.star.stack.connector.StarQueryClient;
import cn.org.dbtools.star.stack.constant.ConstantDef;
import cn.org.dbtools.star.stack.control.manager.StarClusterManager;
import cn.org.dbtools.star.stack.control.manager.ResourceClusterManager;
import cn.org.dbtools.star.stack.dao.ClusterUserMembershipRepository;
import cn.org.dbtools.star.stack.dao.CoreUserRepository;
import cn.org.dbtools.star.stack.dao.PermissionsGroupMembershipRepository;
import cn.org.dbtools.star.stack.dao.PermissionsGroupRoleRepository;
import cn.org.dbtools.star.stack.driver.JdbcSampleClient;
import cn.org.dbtools.star.stack.entity.ClusterUserMembershipEntity;
import cn.org.dbtools.star.stack.entity.CoreUserEntity;
import cn.org.dbtools.star.stack.entity.PermissionsGroupRoleEntity;
import cn.org.dbtools.star.stack.entity.SettingEntity;
import cn.org.dbtools.star.stack.exception.StarSpaceDuplicatedException;
import cn.org.dbtools.star.stack.exception.NoAdminPermissionException;
import cn.org.dbtools.star.stack.model.request.user.UserGroupRole;
import cn.org.dbtools.star.stack.service.BaseService;
import cn.org.dbtools.star.stack.service.config.ConfigConstant;
import cn.org.dbtools.star.stack.service.construct.MetadataService;
import cn.org.dbtools.star.stack.util.CredsUtil;
import cn.org.dbtools.star.stack.util.ListUtil;
import cn.org.dbtools.star.stack.util.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * @Description：star Manager Create user Star cluster space when servicing
 */
@Component
@Slf4j
public class StarManagerUserSpaceComponent extends BaseService {

    public static final String ADMIN_USER_NAME = "Administrators_";

    public static final String ALL_USER_NAME = "All Users_";

    private static final String STAR_ANALYZER_USER_NAME = "Analyzer";

    @Autowired
    private CoreUserRepository userRepository;

    @Autowired
    private PermissionsGroupRoleRepository groupRoleRepository;

    @Autowired
    private ClusterInfoRepository clusterInfoRepository;

    @Autowired
    private ResourceClusterManager resourceClusterManager;

    @Autowired
    private StarLoginClient starLoginClient;

    @Autowired
    private ClusterUserComponent clusterUserComponent;

    @Autowired
    private SettingComponent settingComponent;

    @Autowired
    private PermissionsGroupMembershipRepository membershipRepository;

    @Autowired
    private MetadataService managerMetadataService;

    @Autowired
    private ManagerMetaSyncComponent managerMetaSyncComponent;

    @Autowired
    private JdbcSampleClient jdbcClient;

    @Autowired
    private StarQueryClient queryClient;

    @Autowired
    private ModelControlRequestRepository requestRepository;

    @Autowired
    private ClusterUserMembershipRepository clusterUserMembershipRepository;

    // TODO:It will be removed later
    @Autowired
    private StarClusterManager clusterManager;

    public long create(NewUserSpaceCreateReq createReq, String userName) throws Exception {
        log.debug("Super user create star user space.");
        // Verify whether the initialization of the space authentication method is completed
        SettingEntity enabled = settingComponent.readSetting(ConfigConstant.AUTH_TYPE_KEY);

        if (enabled == null || StringUtils.isEmpty(enabled.getValue())) {
            log.debug("The auth type not be inited.");
            throw new StudioNotInitException();
        }

        checkRequestBody(createReq.hasEmptyFieldNoCluster());

        // check space name
        nameCheck(createReq.getName());

        checkAdminUserList(createReq.getSpaceAdminUsers());

        // create space information
        ClusterInfoEntity clusterInfoInit = null;

        if (createReq.getCluster() != null) {
            log.debug("Add star cluster info.");
            clusterInfoInit = validateCluster(createReq.getCluster());
        } else {
            log.debug("Skip add star cluster info.");
            clusterInfoInit = new ClusterInfoEntity();
        }
        clusterInfoInit.setName(createReq.getName());
        clusterInfoInit.setDescription(createReq.getDescribe());
        clusterInfoInit.setCreateTime(new Timestamp(System.currentTimeMillis()));
        clusterInfoInit.setCreator(userName);

        ClusterInfoEntity clusterInfoSave = clusterInfoRepository.save(clusterInfoInit);

        // Initialize the permission group management information of the space
        initSpaceAdminPermisssion(createReq.getSpaceAdminUsers(), clusterInfoSave);

        // Initialize the correspondence between permission group and Star virtual user
        if (createReq.getCluster() != null) {
            initGroupStarUser(clusterInfoSave);
        }

        // Synchronize Star metadata
        managerMetadataService.syncMetadataByCluster(clusterInfoSave);

        // Send invitation mail
        String joinUrl = "";
//        mailComponent.sendInvitationMail(createReq.getUser().getEmail(),
//                AuthenticationService.SUPER_USER_NAME_VALUE, joinUrl);

        return clusterInfoSave.getId();
    }

    public ClusterInfoEntity validateCluster(ClusterCreateReq createReq) throws Exception {
        log.debug("validate star cluster info.");
        checkRequestBody(createReq.hasEmptyField());
        log.info("Verify that the Star cluster already exists.");
        List<ClusterInfoEntity> exsitEntities =
                clusterInfoRepository.getByAddressAndPort(createReq.getAddress(), createReq.getHttpPort());
        if (exsitEntities != null && exsitEntities.size() != 0) {
            log.error("The star cluster {} is already associated with space.", createReq.getAddress() + ":"
                    + createReq.getHttpPort());
            throw new StarSpaceDuplicatedException();
        }

        log.info("Verify that the Star cluster is available");
        ClusterInfoEntity entity = new ClusterInfoEntity();
        entity.updateByClusterInfo(createReq);
        // encrypt passwd
        entity.setPasswd(CredsUtil.aesEncrypt(entity.getPasswd()));
        // Just verify whether the Star HTTP interface can be accessed
        try {
            starLoginClient.loginStar(entity);
        } catch (Exception e) {
            log.error("Star cluster jdbc access error.");
            log.error("Star cluster jdbc addr:{}, port:{}, user:{}, pass:{}",
                    createReq.getAddress(), createReq.getQueryPort(),
                    createReq.getUser(), createReq.getPasswd());
            throw new StarJdbcPortErrorException();
        }

        // The manager function is enabled by default
        entity.setManagerEnable(true);
        return entity;
    }

    /**
     * Change cluster space information
     * 1. Only modify the space name and other information;
     * 2. Add new cluster connection information;
     * TODO: Currently, modifying cluster information is not supported
     *
     * @param user
     * @param spaceId
     * @param updateReq
     * @return
     * @throws Exception
     */
    public NewUserSpaceInfo update(CoreUserEntity user, long spaceId, NewUserSpaceCreateReq updateReq) throws Exception {
        checkRequestBody(updateReq.hasEmptyFieldNoCluster());

        log.debug("update space {} information.", spaceId);

        ClusterInfoEntity clusterInfo = clusterUserComponent.checkUserClusterAdminPermission(user, spaceId);

        if (!clusterInfo.getName().equals(updateReq.getName())) {
            nameCheck(updateReq.getName());
        }
        log.debug("The name not be changed");
        clusterInfo.setName(updateReq.getName());
        clusterInfo.setDescription(updateReq.getDescribe());
        clusterInfo.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        log.debug("update space info.");

        // Determine whether the cluster information needs to be changed
        if (updateReq.getCluster() != null) {
            // TODO: if the user name and password of Star cluster already exist, they cannot be modified
            if (clusterInfo.getAddress() != null) {
                log.error("The space star cluster information already exists.");
                throw new StarSpaceDuplicatedException();
            }
            clusterAccess(updateReq.getCluster(), clusterInfo);
        }

        // admin user update admin userList
        if (user.isSuperuser()) {
            checkAdminUserList(updateReq.getSpaceAdminUsers());
            log.debug("Admin user update cluster admin user list.");
            // Get the original space administrator list
            List<Integer> oldAdminUsers = membershipRepository.getUserIdsByGroupId(clusterInfo.getAdminGroupId());
            log.debug("Old admin users {}", oldAdminUsers);

            log.debug("New admin users {}", updateReq.getSpaceAdminUsers());

            // Get the new administrator role user in the space
            List<Integer> addAdminUsers = ListUtil.getAddList(updateReq.getSpaceAdminUsers(), oldAdminUsers);
            log.debug("Add admin users {}", addAdminUsers);
            for (Integer adminUserId : addAdminUsers) {
                if (adminUserId < 0) {
                    continue;
                }
                // Judge whether the user is in the current space
                List<ClusterUserMembershipEntity> membershipEntities =
                        clusterUserMembershipRepository.getByUserIdAndClusterId(adminUserId, spaceId);
                if (membershipEntities.isEmpty()) {
                    // Users are not in the space yet. They are added to the space
                    // and also to the administrator and alluser user groups
                    clusterUserComponent.addAdminUserToCluster(adminUserId, clusterInfo);
                } else {
                    // The user is already in the space, but not an administrator,
                    // and is added to the administrator group
                    clusterUserComponent.addGroupUserMembership(adminUserId, clusterInfo.getAdminGroupId());
                }
            }

            // Get the administrator role user for space deletion
            List<Integer> reduceAdminUsers = ListUtil.getReduceList(updateReq.getSpaceAdminUsers(), oldAdminUsers);
            log.debug("Reduce admin users {}", reduceAdminUsers);
            for (Integer adminUserId : reduceAdminUsers) {
                if (adminUserId < 0) {
                    continue;
                }
                // Delete the user from the space administrator role
                membershipRepository.deleteByUserIdAndGroupId(clusterInfo.getAdminGroupId(), adminUserId);

                // also delete the user form all user role and cluster space
                membershipRepository.deleteByUserIdAndGroupId(clusterInfo.getAllUserGroupId(), adminUserId);
                clusterUserMembershipRepository.deleteByUserIdAndClusterId(adminUserId, clusterInfo.getId());
            }

        }
        ClusterInfoEntity clusterInfoSave = clusterInfoRepository.save(clusterInfo);
        NewUserSpaceInfo userSpaceInfo = clusterInfoSave.transToNewModel();
        getAdminGroupUserList(userSpaceInfo, clusterInfo.getAdminGroupId());
        return userSpaceInfo;
    }

    public void clusterAccess(ClusterCreateReq clusterAccessInfo, ClusterInfoEntity clusterInfo) throws Exception {
        checkRequestBody(clusterAccessInfo.hasEmptyField());

        String newAddress = clusterAccessInfo.getAddress();
        String oldAddress = clusterInfo.getAddress();

        if (oldAddress == null) {
            log.debug("First access to star cluster information in space");
        } else if (newAddress.equals(oldAddress)) {
            log.debug("The space has been connected to the cluster information, "
                    + "and the cluster information has not changed");
            return;
        } else {
            log.debug("The space has been connected to the cluster information, "
                    + "and the cluster information has changed");
            deleteClusterPermissionInfo(clusterInfo);
        }

        updateAccessInfo(clusterAccessInfo, clusterInfo);
    }

    private void updateAccessInfo(ClusterCreateReq clusterAccessInfo,
                                  ClusterInfoEntity clusterInfo) throws Exception {
        validateCluster(clusterAccessInfo);

        clusterInfo.updateByClusterInfo(clusterAccessInfo);
        clusterInfo.setPasswd(CredsUtil.aesEncrypt(clusterInfo.getPasswd()));
        clusterInfo.setStatus(ClusterInfoEntity.AppClusterStatus.NORMAL.name());

        // Initialize the correspondence between permission group and Star virtual user
        initGroupStarUser(clusterInfo);

        // Synchronize Star metadata
        managerMetadataService.syncMetadataByCluster(clusterInfo);
        clusterInfoRepository.save(clusterInfo);
    }

    public boolean nameCheck(String name) throws Exception {
        log.debug("Check cluster name {} is duplicate.", name);
        List<ClusterInfoEntity> clusterInfos = clusterInfoRepository.getByName(name);
        if (clusterInfos != null && clusterInfos.size() != 0) {
            log.error("The space name {} already exists.", name);
            throw new NameDuplicatedException();
        }
        return true;
    }

    /**
     * Get the list of spaces for which the user has permission
     *
     * @param userEntity
     * @return
     */
    public List<NewUserSpaceInfo> getAllSpaceByUser(CoreUserEntity userEntity) {
        int userId = userEntity.getId();
        log.debug("User {} get space list", userId);

        if (userEntity.isSuperuser()) {
            log.debug("Admin user get all space list.");
            List<ClusterInfoEntity> clusterInfos = clusterInfoRepository.findAll(Sort.by("id").ascending());
            List<NewUserSpaceInfo> spaceInfos = Lists.newArrayList();
            for (ClusterInfoEntity clusterInfo : clusterInfos) {

                NewUserSpaceInfo spaceInfo = clusterInfo.transToNewModel();
                getClusterRequestInfo(spaceInfo, clusterInfo.getId());
                getAdminGroupUserList(spaceInfo, clusterInfo.getAdminGroupId());
                spaceInfos.add(spaceInfo);
            }
            return spaceInfos;
        } else {
            log.debug("user get all space list.");
            List<ClusterUserMembershipEntity> userMembershipEntities = clusterUserMembershipRepository.getByUserId(userId);
            List<NewUserSpaceInfo> spaceInfos = Lists.newArrayList();
            for (ClusterUserMembershipEntity clusterUserMembershipEntity : userMembershipEntities) {
                long clusterId = clusterUserMembershipEntity.getClusterId();
                ClusterInfoEntity clusterInfoEntity = clusterInfoRepository.findById(clusterId).get();
                NewUserSpaceInfo spaceInfo = clusterInfoEntity.transToNewModel();
                getClusterRequestInfo(spaceInfo, clusterInfoEntity.getId());
                if (!spaceInfo.isRequestCompleted()) {
                    continue;
                }
                spaceInfos.add(spaceInfo);
            }
            return spaceInfos;
        }
    }

    public NewUserSpaceInfo getById(CoreUserEntity user, int spaceId) throws Exception {
        log.debug("User {} get space {} info.", user.getId(), spaceId);
        ClusterInfoEntity clusterInfo = clusterUserComponent.checkUserClusterAdminPermission(user, spaceId);
        setClusterStatus(clusterInfo);

        NewUserSpaceInfo result = clusterInfo.transToNewModel();
        getClusterRequestInfo(result, spaceId);

        if (!result.isRequestCompleted() && !user.isSuperuser()) {
            log.error("Ordinary users do not have permission to view unfinished space");
            throw new NoAdminPermissionException();
        }

        getAdminGroupUserList(result, clusterInfo.getAdminGroupId());

        return result;
    }

    private void setClusterStatus(ClusterInfoEntity clusterInfo) {
        try {
            jdbcClient.testConnetion(clusterInfo.getAddress(), clusterInfo.getQueryPort(),
                    ConstantDef.MYSQL_DEFAULT_SCHEMA, clusterInfo.getUser(), CredsUtil.tryAesDecrypt(clusterInfo.getPasswd()));
            clusterInfo.setStatus(ClusterInfoEntity.AppClusterStatus.NORMAL.name());
        } catch (Exception e) {
            clusterInfo.setStatus(ClusterInfoEntity.AppClusterStatus.ABNORMAL.name());
        }
    }

    /**
     * Delete a space's information
     * 1. Information of space itself
     * 2. Space permissions and user group information
     * 3. User  of space
     *
     * @param spaceId
     * @throws Exception
     */
    public void deleteSpace(long spaceId) throws Exception {
        ClusterInfoEntity clusterInfo = clusterInfoRepository.findById(spaceId).get();

        // delete cluster information
        clusterInfoRepository.deleteById(spaceId);

        // delete cluster configuration
        log.debug("delete cluster {} config infos.", spaceId);
        settingComponent.deleteAdminSetting(spaceId);

        deleteClusterPermissionInfo(clusterInfo);

        // delete user information
        log.debug("delete cluster {} all user membership.", spaceId);
        clusterUserMembershipRepository.deleteByClusterId(spaceId);

        // TODO: In order to be compatible with the deleted content of spatial information before, it is put here.
        //  If the interface that releases both cluster and physical resources is implemented later,
        //  it will be unified in the current starcluster processing operation
        clusterManager.deleteClusterOperation(clusterInfo);

        if (clusterInfo.getResourceClusterId() < 1L) {
            log.info("resource cluster has not been created");
            return;
        }
        resourceClusterManager.deleteAgentsOperation(clusterInfo.getResourceClusterId());
        resourceClusterManager.deleteOperation(clusterInfo.getResourceClusterId());
    }

    private void deleteClusterPermissionInfo(ClusterInfoEntity clusterInfo) throws Exception {
        long spaceId = clusterInfo.getId();
        log.debug("delete cluster {} data permission.", spaceId);
        // Delete the star jdbc agent account
        PermissionsGroupRoleEntity allUserGroup = groupRoleRepository.findById(clusterInfo.getAllUserGroupId()).get();

        // Get all user groups in the cluster
        HashSet<Integer> groupIds = groupRoleRepository.getGroupIdByClusterId(spaceId);

        // Delete permission user group and delete the mapping relationship between user and group
        log.debug("delete cluster {} all permission group and group user membership.", spaceId);
        for (int groupId : groupIds) {
            groupRoleRepository.deleteById(groupId);
            membershipRepository.deleteByGroupId(groupId);
        }

        // Only after the cluster accesses the access information
        if (clusterInfo.getAddress() != null && !clusterInfo.getAddress().isEmpty()) {
            log.debug("delete cluster {} manager metadata and data permission.", spaceId);
            managerMetaSyncComponent.deleteClusterMetadata(clusterInfo);

            log.debug("Delete cluster {} analyzer user {}.", spaceId, allUserGroup.getStarUserName());
            queryClient.deleteUser(ConstantDef.STAR_DEFAULT_NS, ConstantDef.MYSQL_DEFAULT_SCHEMA, clusterInfo,
                    allUserGroup.getStarUserName());
        }

        // After deleting the user's space, set clusterid to 0
        List<CoreUserEntity> users = userRepository.getByClusterId(spaceId);
        for (CoreUserEntity user : users) {
            user.setClusterId(0L);
            userRepository.save(user);
        }
    }

    private void getAdminGroupUserList(NewUserSpaceInfo userSpaceInfo, int adminGroup) {
        List<Integer> adminUsers = membershipRepository.getUserIdsByGroupId(adminGroup);
        List<NewUserSpaceInfo.SpaceAdminUserInfo> adminUserInfos = Lists.newArrayList();
        List<Integer> idList = Lists.newArrayList();
        for (Integer userId : adminUsers) {
            if (userId < 0) {
                continue;
            }
            CoreUserEntity userEntity = userRepository.findById(userId).get();
            adminUserInfos.add(userEntity.castToAdminUserInfo());
            idList.add(userId);
        }

        userSpaceInfo.setSpaceAdminUser(adminUserInfos);
        userSpaceInfo.setSpaceAdminUserId(idList);
    }

    // Get the creation or takeover request of cluster space
    private void getClusterRequestInfo(NewUserSpaceInfo spaceInfo, long clusterId) {
        List<ModelControlRequestEntity> requestEntities =
                requestRepository.getByModelLevelAndIdAndCompleted(ModelControlLevel.STAR_CLUSTER,
                        clusterId, false);
        if (requestEntities.isEmpty()) {
            spaceInfo.setRequestCompleted(true);
        } else {
            ModelControlRequestEntity requestEntity = requestEntities.get(0);
            // TODO: Get the creation or takeover request of cluster space
            if (ModelControlRequestType.CREATION.equals(requestEntity.getRequestType())
                    || ModelControlRequestType.TAKE_OVER.equals(requestEntity.getRequestType())) {
                spaceInfo.setRequestCompleted(false);
                spaceInfo.setRequestId(requestEntity.getId());
                spaceInfo.setRequestInfo(JSON.parse(requestEntity.getRequestInfo()));
                spaceInfo.setEventType(requestEntity.getCurrentEventType());
            } else {
                spaceInfo.setRequestCompleted(true);
            }
        }
    }

    private void checkAdminUserList(List<Integer> adminUsers) throws Exception {
        // If the user list does not exist, an error is returned
        if (adminUsers == null || adminUsers.isEmpty()) {
            log.error("The admin user list empty.");
            throw new RequestFieldNullException();
        }

        // check user exist
        for (Integer user : adminUsers) {
            if (userRepository.findById(user).equals(Optional.empty())) {
                log.error("The admin user {} not exist.", user);
                throw new UserNotExistException();
            }
        }
    }

    /**
     * Initialize the Star cluster user and password corresponding to the space user group
     *
     * @param clusterInfo
     */
    private void initGroupStarUser(ClusterInfoEntity clusterInfo) throws Exception {
        log.debug("Star cluster validate，init group star user.");

        // The administrator group directly initializes the admin privileged
        PermissionsGroupRoleEntity adminGroup = groupRoleRepository.findById(clusterInfo.getAdminGroupId()).get();
        adminGroup.setStarUserName(clusterInfo.getUser());
        adminGroup.setPassword(clusterInfo.getPasswd());
        groupRoleRepository.save(adminGroup);

        // create select user in Star for manager jdbc access
        PermissionsGroupRoleEntity allUserGroup = groupRoleRepository.findById(clusterInfo.getAllUserGroupId()).get();

        String userName = STAR_ANALYZER_USER_NAME + UuidUtil.newUuidString();
        String password = queryClient.createUser(ConstantDef.STAR_DEFAULT_NS, ConstantDef.MYSQL_DEFAULT_SCHEMA,
                clusterInfo, userName);
        allUserGroup.setStarUserName(userName);
        allUserGroup.setPassword(CredsUtil.aesEncrypt(password));

        groupRoleRepository.save(allUserGroup);
        log.debug("save star user for group");
    }

    /**
     * Initialize permission groups in space
     *
     * @param userIds
     * @param clusterInfoEntity
     */
    private void initSpaceAdminPermisssion(List<Integer> userIds, ClusterInfoEntity clusterInfoEntity) {
        long clusterId = clusterInfoEntity.getId();
        log.debug("Init star user cluster {} admin user {} permissions.", clusterId, userIds);

        // Each space needs to create two user groups, admin and all user
        // Create space admin group
        int adminGroupId = clusterUserComponent.addPermissionsGroup(ADMIN_USER_NAME + clusterId,
                clusterId, UserGroupRole.Administrator);
        log.debug("Init star user space {} admin group is {}.", clusterId, adminGroupId);

        // Create space all user group
        int allUserGroupId = clusterUserComponent.addPermissionsGroup(ALL_USER_NAME + clusterId,
                clusterId, UserGroupRole.Analyzer);
        log.debug("Init star user space {} all user group is {}.", clusterId, allUserGroupId);

        clusterInfoEntity.setAdminGroupId(adminGroupId);
        clusterInfoEntity.setAllUserGroupId(allUserGroupId);

        clusterInfoRepository.save(clusterInfoEntity);

        // Initialize the relationship between permission groups and users in the space
        for (Integer userId : userIds) {
            clusterUserComponent.addAdminUserToCluster(userId, clusterInfoEntity);
        }

        log.debug("Save star user space user and group.");
    }
}
