// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.control.request.handler;

import cn.org.dbtools.star.stack.control.request.StarClusterRequestHandler;
import cn.org.dbtools.star.stack.dao.ClusterInfoRepository;
import cn.org.dbtools.star.stack.dao.ResourceNodeRepository;
import cn.org.dbtools.star.stack.entity.ClusterInfoEntity;
import cn.org.dbtools.star.stack.entity.ResourceNodeEntity;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import cn.org.dbtools.star.manager.common.util.ServerAndAgentConstant;
import cn.org.dbtools.star.stack.control.ModelControlResponse;
import cn.org.dbtools.star.stack.control.manager.StarClusterManager;
import cn.org.dbtools.star.stack.control.manager.ResourceClusterManager;
import cn.org.dbtools.star.stack.control.request.StarClusterRequest;
import cn.org.dbtools.star.stack.control.request.content.StarClusterTakeOverRequest;
import cn.org.dbtools.star.stack.driver.JdbcSampleClient;
import cn.org.dbtools.star.stack.entity.CoreUserEntity;
import cn.org.dbtools.star.stack.model.request.control.StarClusterModuleResourceConfig;
import cn.org.dbtools.star.stack.util.CredsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// TODO: Subsequent improvement of request content judgment and exception handling
@Slf4j
@Component
public class StarClusterTakeOverRequestHandler extends StarClusterRequestHandler {

    @Autowired
    private StarClusterManager starClusterManager;

    @Autowired
    private ResourceClusterManager resourceClusterManager;

    @Autowired
    private ClusterInfoRepository clusterInfoRepository;

    @Autowired
    private ResourceNodeRepository nodeRepository;

    @Autowired
    private JdbcSampleClient jdbcClient;

    @Override
    public ModelControlResponse handleRequestEvent(CoreUserEntity user, StarClusterRequest takeOverRequest,
                                                   boolean modelInit) throws Exception {
        StarClusterTakeOverRequest request = (StarClusterTakeOverRequest) takeOverRequest;
        switch (request.getEventType()) {
            case 1: // CREATE_CLUSTER_SPACE
                return handleCreateClusterSpaceEvent(user, request, modelInit);
            case 2: // ACCESS_STAR_CLUSTER
                return handleAccessStarClusterEvent(user, request);
            case 3: // CREATE_AND_START_RESOURCE_CLUSTER
                return handleCreateAndStartResourceClusterEvent(user, request);
            case 4: // CHECK_CLUSTER_DEPLOY
                return handleCheckClusterDeployEvent(user, request);
            case 5: // COMPLETED_TAKEOVER
                return handleCompletedEvent(user, request);
            default:
                log.error("Event type error.");
                throw new Exception("Event type error.");
        }
    }

    @Override
    public long initRequestModel(StarClusterRequest request, String creator) throws Exception {
        log.info("init star cluster, create cluster user space");
        StarClusterTakeOverRequest takeOverRequest = (StarClusterTakeOverRequest) request;
        return starClusterManager.initOperation(takeOverRequest.getReqInfo().getSpaceInfo(), creator);
    }

    // CREATE_CLUSTER_SPACE
    private ModelControlResponse handleCreateClusterSpaceEvent(CoreUserEntity user,
                                                               StarClusterTakeOverRequest request,
                                                               boolean isInit) throws Exception {
        long clusterId = request.getClusterId();
        log.info("handle take over cluster {} CREATE_CLUSTER_SPACE request {} event", clusterId, request.getRequestId());
        if (!isInit) {
            log.info("The cluster user space already exist, update info.");
            starClusterManager.updateClusterOperation(user, clusterId,
                    request.getReqInfo().getSpaceInfo());
        }

        return getResponse(request, false);
    }

    // ACCESS_STAR_CLUSTER
    private ModelControlResponse handleAccessStarClusterEvent(CoreUserEntity user,
                                                               StarClusterTakeOverRequest request) throws Exception {
        long clusterId = request.getClusterId();
        log.info("handle take over cluster {} ACCESS_STAR_CLUSTER request {} event", clusterId, request.getRequestId());

        starClusterManager.clusterAccessOperation(clusterId, request.getReqInfo().getClusterAccessInfo());

        return getResponse(request, false);
    }

    // CREATE_AND_START_RESOURCE_CLUSTER
    private ModelControlResponse handleCreateAndStartResourceClusterEvent(CoreUserEntity user,
                                                                          StarClusterTakeOverRequest request) throws Exception {
        long clusterId = request.getClusterId();
        log.info("handle take over cluster {} CREATE_AND_START_RESOURCE_CLUSTER request {} event",
                clusterId, request.getRequestId());

        ClusterInfoEntity clusterInfo = clusterInfoRepository.findById(clusterId).get();

        // TODO:get cluster nodes ip info
        List<String> nodeIps = new ArrayList<>();

        Statement stmt = jdbcClient.getStatement(clusterInfo.getAddress(), clusterInfo.getQueryPort(),
                clusterInfo.getUser(), CredsUtil.aesDecrypt(clusterInfo.getPasswd()));
        Set<String> feNodeIps = jdbcClient.getFeOrBeIps(stmt, "'/frontends';");
        log.debug("The node list IP of Star cluster Fe is {}", feNodeIps);

        Set<String> beNodeIps = jdbcClient.getFeOrBeIps(stmt, "'/backends';");
        log.debug("The node list IP of Star cluster Be is {}", beNodeIps);

        Set<String> brokerNodeIps = jdbcClient.getFeOrBeIps(stmt, "'/brokers';");
        log.debug("The node list IP of Star cluster Broker is {}", brokerNodeIps);

        jdbcClient.closeStatement(stmt);

        Set<String> allNodeDistinct = new HashSet<>();
        allNodeDistinct.addAll(feNodeIps);
        allNodeDistinct.addAll(beNodeIps);
        allNodeDistinct.addAll(brokerNodeIps);

        log.debug("The node list distinct IP of Star cluster is {}", allNodeDistinct);

        nodeIps.addAll(allNodeDistinct);
        log.debug("The node list IP of Star cluster is {}", nodeIps);

        starClusterManager.createClusterResourceOperation(user, clusterInfo, request.getReqInfo().getAuthInfo(), nodeIps);
        starClusterManager.configClusterResourceOperation(clusterInfo, "", "",
                request.getReqInfo().getInstallInfo(), "", request.getReqInfo().getAgentPort());

        // TODO  sshInfo  and  iplist  can check agent port
        List<ResourceNodeEntity> nodeEntities =
                nodeRepository.getByResourceClusterId(clusterInfo.getResourceClusterId());
        Set<Long> feNodeIds = new HashSet<>();
        Set<Long> beNodeIds = new HashSet<>();
        Set<Long> brokerNodeIds = new HashSet<>();
        for (ResourceNodeEntity nodeEntity : nodeEntities) {
            if (feNodeIps.contains(nodeEntity.getHost())) {
                feNodeIds.add(nodeEntity.getId());
            }

            if (beNodeIps.contains(nodeEntity.getHost())) {
                beNodeIds.add(nodeEntity.getId());
            }

            if (brokerNodeIps.contains(nodeEntity.getHost())) {
                brokerNodeIds.add(nodeEntity.getId());
            }
        }
        log.debug("The node list ID of Star cluster fe is {}", feNodeIds);
        log.debug("The node list ID of Star cluster be is {}", beNodeIds);
        log.debug("The node list ID of Star cluster broker is {}", brokerNodeIds);

        StarClusterModuleResourceConfig feConfig = new StarClusterModuleResourceConfig();
        feConfig.setModuleName(ServerAndAgentConstant.FE_NAME);
        feConfig.setNodeIds(feNodeIds);

        StarClusterModuleResourceConfig beConfig = new StarClusterModuleResourceConfig();
        beConfig.setModuleName(ServerAndAgentConstant.BE_NAME);
        beConfig.setNodeIds(beNodeIds);

        StarClusterModuleResourceConfig brokerConfig = new StarClusterModuleResourceConfig();
        brokerConfig.setModuleName(ServerAndAgentConstant.BROKER_NAME);
        brokerConfig.setNodeIds(brokerNodeIds);

        List<StarClusterModuleResourceConfig> resourceConfigs = Lists.newArrayList(feConfig, beConfig, brokerConfig);

        starClusterManager.scheduleClusterOperation(clusterId, resourceConfigs);

        starClusterManager.startClusterResourceOperation(clusterInfo, request.getRequestId());

        return getResponse(request, false);
    }

    // CHECK_CLUSTER_DEPLOY
    private ModelControlResponse handleCheckClusterDeployEvent(CoreUserEntity user,
                                                               StarClusterTakeOverRequest request) throws Exception {
        long clusterId = request.getClusterId();
        log.info("handle take over cluster {} CHECK_CLUSTER_DEPLOY request {} event", clusterId, request.getRequestId());

        // Check whether the cluster node agent is installed successfully
        ClusterInfoEntity clusterInfoEntity = clusterInfoRepository.findById(clusterId).get();
        resourceClusterManager.checkNodesAgentOperation(clusterInfoEntity.getResourceClusterId());

        starClusterManager.checkClusterDeployOperation(clusterId, request.getRequestId());

        return getResponse(request, false);
    }

    // COMPLETED_TAKEOVER
    private ModelControlResponse handleCompletedEvent(CoreUserEntity user,
                                                      StarClusterTakeOverRequest request) throws Exception {
        long clusterId = request.getClusterId();
        log.info("handle take over cluster {} COMPLETED_TAKEOVER request {} event", clusterId, request.getRequestId());
        starClusterManager.checkClusterInstancesOperation(clusterId);
        return getResponse(request, true);
    }
}
