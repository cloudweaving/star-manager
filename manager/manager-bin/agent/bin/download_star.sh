#!/bin/bash
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

download_func() {

    FE_Image=${1}
    BE_Image=${2}
    srHome=${3}
    dataHome=${4}
    FE_LEADER_IP=${5}
    echo srHome=$srHome,dataHome=$dataHome,FE_LEADER_IP=$FE_LEADER_IP,FE_Image=$FE_Image,BE_Image=$BE_Image
    source /etc/profile
    source ~/.bash_profile
    id sr >/dev/null 2>&1
    if [[ $? -ne 0 ]];then
        useradd sr -u 1000 >/dev/null 2>&1
    fi
    usermod -aG docker sr
    if [[ -e /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor ]];then
        echo 'performance' | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
    fi

    echo '##add##' | tee -a /etc/sysctl.conf

    systemctl status firewalld | grep "running"  2>&1 >/dev/null
    if [ $? -eq 0 ];then
        systemctl stop firewalld.service && systemctl disable firewalld.service
    fi

    om_number=$(cat /proc/sys/vm/overcommit_memory)
    if [ $om_number -ne 1 ];then
        echo 1 | tee /proc/sys/vm/overcommit_memory
        echo vm.overcommit_memory=1 | tee -a /etc/sysctl.conf
    fi

    mmap_number=$(cat /proc/sys/vm/max_map_count)
    if [ "$mmap_number" -lt 200000 ]; then
        echo 262144 > /proc/sys/vm/max_map_count
        echo vm.max_map_count=262144 | tee -a /etc/sysctl.conf
    fi

    enabled_value=$(cat /sys/kernel/mm/transparent_hugepage/enabled | cut -d '[' -f2 | cut -d ']' -f1)
    if [ $enabled_value != "never" ];then
        echo never > /sys/kernel/mm/transparent_hugepage/enabled
        echo 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' >> /etc/rc.d/rc.local
        chmod +x /etc/rc.d/rc.local
    fi

    defrag_value=$(cat /sys/kernel/mm/transparent_hugepage/defrag | cut -d '[' -f2 | cut -d ']' -f1)
    if [ $defrag_value != "never" ];then
        echo never > /sys/kernel/mm/transparent_hugepage/defrag
        echo 'echo never > /sys/kernel/mm/transparent_hugepage/defrag' >> /etc/rc.d/rc.local
    fi

    sed -i 's/SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
    sed -i 's/SELINUXTYPE/#SELINUXTYPE/' /etc/selinux/config
    setenforce 0

    swap_number=$(cat /proc/sys/vm/swappiness)
    if [ $swap_number -ne 0 ];then
        echo 0 | tee /proc/sys/vm/swappiness
        echo vm.swappiness=0 | tee -a /etc/sysctl.conf
    fi
    swap_file=$(free -m | grep -i swap | awk '{print $2}')
    if [ $swap_file -ne 0 ];then
        swapoff -a
        sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
    fi

    ulimit_fs=$(ulimit -n)
    if [ $ulimit_fs -lt 65535 ];then
        echo -e '* soft nofile 655350\n* hard nofile 655350' >> /etc/security/limits.conf
    fi

    ulimit_ps=$(ulimit -u)
    if [ $ulimit_ps -lt 65535 ];then
        echo -e '* soft nproc 655350\n* hard nproc 655350' >> /etc/security/limits.conf
    fi

    tcp_aoo=$(cat /proc/sys/net/ipv4/tcp_abort_on_overflow)
    if [ $tcp_aoo -ne 1 ];then
        echo 1 | tee /proc/sys/net/ipv4/tcp_abort_on_overflow
        echo net.ipv4.tcp_abort_on_overflow=1 | tee -a /etc/sysctl.conf
    fi

    somaxconn_value=$(cat /proc/sys/net/core/somaxconn)
    if [ $somaxconn_value -le 1024 ];then
        echo 1024 | tee /proc/sys/net/core/somaxconn
        echo net.core.somaxconn=1024 | tee -a /etc/sysctl.conf
    fi

    echo 120000 > /proc/sys/kernel/threads-max
    echo 200000 > /proc/sys/kernel/pid_max

    sysctl -p
    source /etc/profile

    mkdir -p ${srHome}
    mkdir -p ${dataHome}/fe/meta
    mkdir -p ${dataHome}/fe/log
    mkdir -p ${dataHome}/be/storage
    mkdir -p ${dataHome}/be/log

    #测试是否有指定镜像 没有就拉取
    if [[ $(docker images | awk '{print $1":"$2}' | grep $FE_Image) == "$FE_Image" ]];then
        echo FE镜像已存在
    else
        echo 拉取FE镜像
        docker pull $FE_Image
        if [[ $(docker images | awk '{print $1":"$2}' | grep $FE_Image) == "$FE_Image" ]];then
            echo FE镜像拉取成功
        else
            echo 拉取FE镜像未完成
        fi
    fi

    if [[ $(docker images | awk '{print $1":"$2}' | grep $BE_Image) == "$BE_Image" ]];then
        echo BE镜像已存在
    else
        echo 拉取BE镜像
        docker pull $BE_Image
        if [[ $(docker images | awk '{print $1":"$2}' | grep $BE_Image) == "$BE_Image" ]];then
            echo BE镜像拉取成功
        else
            echo 拉取BE镜像未完成
        fi
    fi
    #镜像中文件拷出
    cd $srHome && docker run --rm --entrypoint= $FE_Image tar -cf - -C /home/sr/starrocks fe/bin fe/conf | tar -xvf -
    cd $srHome && docker run --rm --entrypoint= $BE_Image tar -cf - -C /home/sr/starrocks be/bin be/conf | tar -xvf -
    chown -R sr:sr $srHome
    chown -R sr:sr $dataHome

    docker create -it --name=starrocks-fe --net=host --restart=unless-stopped \
    -e FE_LEADER_IP=${FE_LEADER_IP} \
    -e LOG_CONSOLE=1 \
    -v ${srHome}/fe/bin:/home/sr/starrocks/fe/bin \
    -v ${srHome}/fe/conf:/home/sr/starrocks/fe/conf \
    -v ${dataHome}/fe/meta:${dataHome}/fe/meta \
    -v ${dataHome}/fe/log:${dataHome}/fe/log \
    ${FE_Image}

    docker create -it --name=starrocks-be --net=host --restart=unless-stopped \
    -e FE_LEADER_IP=${FE_LEADER_IP} \
    -e LOG_CONSOLE=1 \
    -v ${srHome}/be/bin:/home/sr/starrocks/be/bin \
    -v ${srHome}/be/conf:/home/sr/starrocks/be/conf \
    -v ${dataHome}/be/storage:${dataHome}/be/storage \
    -v ${dataHome}/be/log:${dataHome}/be/log \
    ${BE_Image}

    return 1
}

echo "FE_Image is $1, BE_Image is $2,installInfo is $3,dataHome is $4,FE_LEADER_IP is $5"
download_func $1 $2 $3 $4 $5
if [ "$?"x == "0"x ]; then
  exit 1
fi