// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.service.monitor;

import cn.org.dbtools.star.stack.component.ClusterUserComponent;
import cn.org.dbtools.star.stack.connector.StarMonitorClient;
import cn.org.dbtools.star.stack.entity.ClusterInfoEntity;
import cn.org.dbtools.star.stack.model.request.monitor.ClusterMonitorReq;
import lombok.extern.slf4j.Slf4j;
import cn.org.dbtools.star.stack.entity.CoreUserEntity;
import cn.org.dbtools.star.stack.service.user.AuthenticationService;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

@RunWith(JUnit4.class)
@Slf4j
public class StarMonitorServiceTest {
    @InjectMocks
    private StarMonitorService monitorService;

    @Mock
    private AuthenticationService authenticationService;

    @Mock
    private StarMonitorClient starMonitorClient;

    @Mock
    private ClusterUserComponent clusterUserComponent;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test monitoring forwarding interface
     */
    @Test
    public void monitorTest() {
        log.debug("Get star monitor test.");
        int userId = 1;
        long clusterId = 2;

        CoreUserEntity userEntity = mockRequestUser(userId, clusterId);

         // mock cluster
        ClusterInfoEntity clusterInfo = new ClusterInfoEntity();
        clusterInfo.setId(clusterId);
        clusterInfo.setName("star1");
        clusterInfo.setAddress("10.23.32.32");
        clusterInfo.setHttpPort(8030);
        clusterInfo.setQueryPort(8031);
        clusterInfo.setUser("admin");
        clusterInfo.setPasswd("1234");
        clusterInfo.setTimezone("Asia/Shanghai");

        // Testing interfaces that do not require requesting content
        try {
            Mockito.when(authenticationService.checkNewUserAuthWithCookie(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(userEntity);
            Mockito.when(clusterUserComponent.getUserCurrentClusterAndCheckAdmin(userEntity)).thenReturn(clusterInfo);
            monitorService.nodeNum(null, null, null);
            monitorService.disksCapacity(null, null, null);
            monitorService.feList(null, null, null);
            monitorService.beList(null, null, null);
        } catch (Exception e) {
            log.debug("Get star monitor test error.");
            e.printStackTrace();
        }

        // Test the interface that needs to request content
        // mock request
        ClusterMonitorReq monitorReq = new ClusterMonitorReq();
        monitorReq.setStart(1L);
        monitorReq.setEnd(3L);
        monitorReq.setPointNum(3);
        monitorReq.setQuantile("quantile");
        List<String> nodes = Lists.newArrayList("node1", "node2");
        monitorReq.setNodes(nodes);

        // Testing interfaces that do not require requesting content
        try {
            Mockito.when(authenticationService.checkNewUserAuthWithCookie(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(userEntity);
            Mockito.when(clusterUserComponent.getUserCurrentClusterAndCheckAdmin(userEntity)).thenReturn(clusterInfo);
            monitorService.qps(monitorReq, null, null);
            monitorService.queryLatency(monitorReq, null, null);
            monitorService.queryErrRate(monitorReq, null, null);
            monitorService.connTotal(monitorReq, null, null);
            monitorService.txnStatus(monitorReq, null, null);
            monitorService.beCpuUsed(monitorReq, null, null);
            monitorService.beMem(monitorReq, null, null);
            monitorService.beDiskIO(monitorReq, null, null);
            monitorService.beBaseCompactionScore(monitorReq, null, null);
            monitorService.beCumuCompactionScore(monitorReq, null, null);
        } catch (Exception e) {
            log.debug("Get star monitor test error.");
            e.printStackTrace();
        }
    }

    private CoreUserEntity mockRequestUser(int userId, long clusterId) {
        CoreUserEntity userEntity = new CoreUserEntity();
        userEntity.setId(userId);
        userEntity.setClusterId(clusterId);
        userEntity.setFirstName("user");
        userEntity.setIsClusterAdmin(true);
        userEntity.setSuperuser(true);
        return userEntity;
    }
}
