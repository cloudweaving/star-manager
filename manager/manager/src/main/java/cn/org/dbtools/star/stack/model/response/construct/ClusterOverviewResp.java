// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.model.response.construct;

import cn.org.dbtools.star.stack.model.palo.ClusterOverviewInfoExt;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClusterOverviewResp {

    private int dbCount;

    private int tblCount;

    // Cpu occupancy (%)
    private double cpuOccupancy;
    // Ram occupancy (%)
    private double ramOccupancy;
    // Disk occupancy (%)
    private double diskOccupancy;

    private int beCount;

    private int feCount;

    // Amount of disk remaining (GB)
    private double remainDisk;

    public ClusterOverviewResp(ClusterOverviewInfoExt info) {
        this.dbCount = info.getDbCount();
        this.tblCount = info.getTblCount();
        this.diskOccupancy = info.getDiskOccupancy() / (info.getDiskOccupancy() + info.getRemainDisk()) * 100;
        this.feCount = info.getFeCount();
        this.beCount = info.getBeCount();
        this.remainDisk = info.getRemainDisk();
        this.cpuOccupancy = info.getCpuOccupancy();
        this.ramOccupancy = info.getRamOccupancy();
    }
}
