// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.driver;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class StarFieldTypeDef {

    public static final String TYPE_INTEGER = "type/Integer";

    public static final String TYPE_BIGINTEGER = "type/BigInteger";

    public static final String TYPE_FLOAT = "type/Float";

    public static final String TYPE_DECIMAL = "type/Decimal";

    public static final String TYPE_TEXT = "type/Text";

    public static final String TYPE_DATETIME = "type/DateTime";

    public static final String TYPE_DATE = "type/Date";

    public static final String TYPE_BOOLEAN = "type/Boolean";

    public static final String TYPE_ALL = "type/*";

    public static Map<String, String> starFieldTypeMap = new HashMap<>();

    // Star engine field type definition and mapping
    public static Set<String> starFieldType = new HashSet<>();

    static {
        // Star engine field type definition and mapping with structured field types
        starFieldTypeMap.put("TINYINT", TYPE_ALL);
        starFieldTypeMap.put("DATE", TYPE_DATE);
        starFieldTypeMap.put("INT", TYPE_INTEGER);
        starFieldTypeMap.put("SMALLINT", TYPE_INTEGER);
        starFieldTypeMap.put("VARCHAR", TYPE_TEXT);
        starFieldTypeMap.put("DATETIME", TYPE_DATETIME);
        starFieldTypeMap.put("BOOLEAN", TYPE_BOOLEAN);
        starFieldTypeMap.put("BIT", TYPE_BOOLEAN);
        starFieldTypeMap.put("DOUBLE", TYPE_FLOAT);
        starFieldTypeMap.put("FLOAT", TYPE_FLOAT);
        starFieldTypeMap.put("DECIMAL", TYPE_DECIMAL);
        starFieldTypeMap.put("BIGINT", TYPE_BIGINTEGER);
        starFieldTypeMap.put("LARGEINT", TYPE_TEXT); // TODO：At present, largeint is set as text type to solve
                                                      // the problem of precision
        starFieldTypeMap.put("BITMAP", TYPE_ALL);
        starFieldTypeMap.put("CHAR", TYPE_TEXT);
        starFieldTypeMap.put("HLL", TYPE_ALL); // TODO：Currently set HLL as text type
        // Field type of Star engine
        starFieldType.addAll(starFieldTypeMap.keySet());
    }

    /**
     * Get Star mapping data type
     *
     * @param starType starType
     * @return starType
     */
    public static String getStarFieldType(String starType) {
        String type = starType.replaceAll("[^a-zA-Z]", "");
        String result = starFieldTypeMap.get(type);
        if (result == null) {
            return TYPE_ALL;
        }
        return result;
    }
}
