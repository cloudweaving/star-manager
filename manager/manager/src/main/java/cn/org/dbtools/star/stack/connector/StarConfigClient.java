// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.connector;

import cn.org.dbtools.star.stack.constant.ConstantDef;
import cn.org.dbtools.star.stack.entity.ClusterInfoEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class StarConfigClient extends StarClient {

    @Autowired
    private StarQueryClient queryClient;

    private final String FE_NAME = "fe";
    private final String BE_NAME = "be";

    public Map<String, Object> getConfigurationInfo(String type, ClusterInfoEntity entity) throws Exception {

        Map<String, Object> config = new HashMap<>();
        List<String> columnNames = new ArrayList<>();
        columnNames.add("配置项");
        columnNames.add("节点");
        columnNames.add("节点类型");
        columnNames.add("配置值类型");
        columnNames.add("MasterOnly");
        columnNames.add("配置值");
        columnNames.add("可修改");
        config.put("columnNames", columnNames);

        if (FE_NAME.equalsIgnoreCase(type)) {
            config.put("rows", getFeConfigurationInfo(entity));
            return config;
        } else if (BE_NAME.equalsIgnoreCase(type)) {
            config.put("rows", getBeConfigurationInfo(entity));
            return config;

        }
        return null;

    }

    private List<List<String>> getFeConfigurationInfo(ClusterInfoEntity entity) throws Exception {
        String schemaQuery = "ADMIN SHOW FRONTEND CONFIG";
        List<List<String>> result = queryClient.executeSQL(schemaQuery, ConstantDef.STAR_DEFAULT_NS,
                ConstantDef.STAR_DEFAULT_SCHEMA, entity).getData();
        List<List<String>> rows = new ArrayList<>();
        for (List<String> item : result) {
            List<String> row = new ArrayList<>();
            row.add(item.get(0));
            row.add("all");
            row.add("FE");
            row.add(item.get(3));
            row.add("MasterOnly");
            row.add(item.get(2));
            //暂时改为不可修改
//            row.add(item.get(4));
            row.add("false");

            rows.add(row);
        }
        return rows;
    }

    private List<List<String>> getBeConfigurationInfo(ClusterInfoEntity entity) throws Exception {
        List<List<String>> rows = new ArrayList<>();
        return rows;
    }
}
