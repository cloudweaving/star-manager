// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.service.monitor;

import cn.org.dbtools.star.stack.component.ClusterUserComponent;
import cn.org.dbtools.star.stack.entity.ClusterInfoEntity;
import cn.org.dbtools.star.stack.model.palo.ClusterMonitorInfo;
import cn.org.dbtools.star.stack.model.request.monitor.ClusterMonitorReq;
import cn.org.dbtools.star.stack.entity.CoreUserEntity;
import cn.org.dbtools.star.stack.connector.StarMonitorClient;
import cn.org.dbtools.star.stack.service.BaseService;
import cn.org.dbtools.star.stack.service.user.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
@Slf4j
public class StarMonitorService extends BaseService {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private StarMonitorClient starMonitorClient;

    @Autowired
    private ClusterUserComponent clusterUserComponent;

    private ClusterMonitorInfo checkAndHandleCluster(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreUserEntity user = authenticationService.checkNewUserAuthWithCookie(request, response);
        ClusterMonitorInfo info = new ClusterMonitorInfo();

        ClusterInfoEntity clusterInfo = clusterUserComponent.getUserCurrentClusterAndCheckAdmin(user);
        info.setHost(clusterInfo.getAddress());
        info.setHttpPort(clusterInfo.getHttpPort());

        return info;
    }

    /**
     * @param req
     * @param info
     * @return
     */
    private ClusterMonitorInfo assembleInfo(ClusterMonitorReq req, ClusterMonitorInfo info) {
        if (req.getNodes() != null) {
            info.setNodes(req.getNodes());
        }
        info.setStart(req.getStart());
        info.setEnd(req.getEnd());
        if (req.getPointNum() != null) {
            info.setPointNum(req.getPointNum());
        }
        if (!StringUtils.isEmpty(req.getQuantile())) {
            info.setQuantile(req.getQuantile());
        }
        return info;
    }

    public Object nodeNum(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        return starMonitorClient.getNodeNum(info);
    }

    public Object disksCapacity(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        return starMonitorClient.getDisksCapacity(info);
    }

    public Object feList(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        return starMonitorClient.getFeList(info);
    }

    public Object beList(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        return starMonitorClient.getBeList(info);
    }

    public Object qps(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        assembleInfo(req, info);
        return starMonitorClient.getQPS(info);
    }

    public Object queryLatency(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        assembleInfo(req, info);
        return starMonitorClient.getQueryLatency(info);
    }

    public Object queryErrRate(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        assembleInfo(req, info);
        return starMonitorClient.getQueryErrRate(info);
    }

    public Object connTotal(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        assembleInfo(req, info);
        return starMonitorClient.getConnTotal(info);
    }

    public Object txnStatus(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        assembleInfo(req, info);
        return starMonitorClient.getTxnStatus(info);
    }

    public Object beCpuUsed(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        assembleInfo(req, info);
        return starMonitorClient.getBeCpuUsed(info);
    }

    public Object beMem(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        assembleInfo(req, info);
        return starMonitorClient.getBeMem(info);
    }

    public Object beDiskIO(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        assembleInfo(req, info);
        return starMonitorClient.getBeDiskIO(info);
    }

    public Object beBaseCompactionScore(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        assembleInfo(req, info);
        return starMonitorClient.getBeBaseCompactionScore(info);
    }

    public Object beCumuCompactionScore(ClusterMonitorReq req, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClusterMonitorInfo info = checkAndHandleCluster(request, response);
        assembleInfo(req, info);
        return starMonitorClient.getBeCumuCompactionScore(info);
    }

}
