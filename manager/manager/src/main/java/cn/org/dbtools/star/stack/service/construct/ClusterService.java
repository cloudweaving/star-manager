// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.service.construct;

import cn.org.dbtools.star.stack.component.ClusterUserComponent;
import cn.org.dbtools.star.stack.entity.ClusterInfoEntity;
import cn.org.dbtools.star.stack.model.palo.ClusterOverviewInfoExt;
import cn.org.dbtools.star.stack.entity.CoreUserEntity;
import cn.org.dbtools.star.stack.model.response.construct.ClusterOverviewResp;
import cn.org.dbtools.star.stack.connector.StarStatisticClient;
import cn.org.dbtools.star.stack.service.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ClusterService extends BaseService {

    private ClusterUserComponent clusterUserComponent;

    private StarStatisticClient statisticClient;

    @Autowired
    public ClusterService(ClusterUserComponent clusterUserComponent,
                          StarStatisticClient statisticClient) {
        this.clusterUserComponent = clusterUserComponent;
        this.statisticClient = statisticClient;
    }

    /**
     * Obtain the operation and maintenance information of the user's Star cluster
     * @return
     */
    public ClusterOverviewResp getClusterOpInfo(CoreUserEntity user) throws Exception {

        ClusterInfoEntity clusterInfo = clusterUserComponent.getUserCurrentClusterAndCheckAdmin(user);
        ClusterOverviewInfoExt result = statisticClient.getClusterInfo(clusterInfo);

        ClusterOverviewResp resp = new ClusterOverviewResp(result);
        log.info("Get Star cluster info success.");
        return resp;
    }
}
