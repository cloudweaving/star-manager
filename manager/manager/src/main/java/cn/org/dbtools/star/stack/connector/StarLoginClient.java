// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.stack.connector;

import cn.org.dbtools.star.stack.driver.JdbcSampleClient;
import cn.org.dbtools.star.stack.entity.ClusterInfoEntity;
import cn.org.dbtools.star.stack.util.CredsUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class StarLoginClient extends StarClient {
    protected HttpClientPoolManager poolManager;

    @Autowired
    private JdbcSampleClient jdbcClient;

    @Autowired
    public StarLoginClient(HttpClientPoolManager poolManager) {
        this.poolManager = poolManager;
    }

    public boolean loginStar(ClusterInfoEntity entity) throws Exception {
        // Just verify whether the Star JDBC protocol can be accessed
        // 当地址中刚出现多个ip段组装（,分割）的时候，只需要取其中一个地址进行验证即可，默认取第一个
        String host = entity.getAddress();
        String passwd = CredsUtil.aesDecrypt(entity.getPasswd());
        if (entity.getAddress().contains(",")) {
            host = entity.getAddress().split(",")[0];
        }
        log.info("Star cluster jdbc addr:{}, port:{}, user:{}, pass:{}",
                entity.getAddress(), entity.getQueryPort(), entity.getUser(), passwd);
        return jdbcClient.testConnetion(host, entity.getQueryPort(), entity.getUser(), passwd);
    }
}
