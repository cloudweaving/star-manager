// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package cn.org.dbtools.star.manager.common.util;

import com.google.common.collect.Maps;

import java.util.Map;

public class ServerAndAgentConstant {

    private ServerAndAgentConstant() {
        throw new UnsupportedOperationException();
    }

    public static final String USER_ADMIN = "admin";
    public static final String USER_ROOT = "root";

    // TODO:Later, it will be defined uniformly through the cluster module template
    public static final String FE_NAME = "fe";
    public static final String BE_NAME = "be";
    public static final String BROKER_NAME = "broker";
    public static final String AGENT_NAME = "agent";

    // The path of borker module initialization when the installation package is downloaded
    public static final String BROKER_INIT_SUB_DIR = "apache_hdfs_broker";
    public static final String BAIDU_BROKER_INIT_SUB_DIR = "baidu_star_broker";

    public static final String STAR_INSTALL_HOME_EVN = "STAR_INSTALL_HOME";
    public static final String STAR_PACKAGE_URL_ENV = "STAR_PACKAGE_URL";

    public static final String FE_PID_FILE = "fe.pid";
    public static final String BE_PID_FILE = "be.pid";
    public static final String BROKER_PID_FILE = "apache_hdfs_broker.pid";
    public static final String BAIDU_BROKER_PID_FILE = "baidu_star_broker.pid";

    public static final String FE_PID_NAME = "StarRocksFE";
    public static final String BE_PID_NAME = "starrocks_be";
    public static final String BROKER_PID_NAME = "BrokerBootstrap";

    private static final String FE_START_SHELL_ENV = "FE_START_SHELL";
    private static final String BE_START_SHELL_ENV = "BE_START_SHELL";
    private static final String BROKER_START_SHELL_ENV = "BROKER_START_SHELL";
    private static final String FE_STOP_SHELL_ENV = "FE_STOP_SHELL";
    private static final String BE_STOP_SHELL_ENV = "BE_STOP_SHELL";
    private static final String BROKER_STOP_SHELL_ENV = "BROKER_STOP_SHELL";
    private static final String IS_ON_DOCKER_ENV = "RUN_DOCKER";

    // TODO 增加在环境变量中取启停脚本名称的方式
    public static final String FE_START_SCRIPT = "start_fe_container.sh";

    public static String getFeStartScript() {
        String feStartScript = System.getenv(FE_START_SHELL_ENV);
        return feStartScript != null && !feStartScript.isEmpty() && !"true".equals(System.getenv(IS_ON_DOCKER_ENV)) ? feStartScript : FE_START_SCRIPT;
    }

    public static final String BE_START_SCRIPT = "start_be_container.sh";

    public static String getBeStartScript() {
        String beStartScript = System.getenv(BE_START_SHELL_ENV);
        return beStartScript != null && !beStartScript.isEmpty() && !"true".equals(System.getenv(IS_ON_DOCKER_ENV)) ? beStartScript : BE_START_SCRIPT;
    }

    public static final String BROKER_START_SCRIPT = "start_broker_container.sh";

    public static String getBrokerStartScript() {
        String brokerStartScript = System.getenv(BROKER_START_SHELL_ENV);
        return brokerStartScript != null && !brokerStartScript.isEmpty() && !"true".equals(System.getenv(IS_ON_DOCKER_ENV)) ? brokerStartScript : BROKER_START_SCRIPT;
    }

    public static final String FE_STOP_SCRIPT = "stop_fe_container.sh";

    public static String getFeStopScript() {
        String feStopScript = System.getenv(FE_STOP_SHELL_ENV);
        return feStopScript != null && !feStopScript.isEmpty() && !"true".equals(System.getenv(IS_ON_DOCKER_ENV)) ? feStopScript : FE_STOP_SCRIPT;
    }

    public static final String BE_STOP_SCRIPT = "stop_be_container.sh";

    public static String getBeStopScript() {
        String beStopScript = System.getenv(BE_STOP_SHELL_ENV);
        return beStopScript != null && !beStopScript.isEmpty() && !"true".equals(System.getenv(IS_ON_DOCKER_ENV)) ? beStopScript : BE_STOP_SCRIPT;
    }

    public static final String BROKER_STOP_SCRIPT = "stop_broker_container.sh";

    public static String getBrokerStopScript() {
        String brokerStopScript = System.getenv(BROKER_STOP_SHELL_ENV);
        return brokerStopScript != null && !brokerStopScript.isEmpty() && !"true".equals(System.getenv(IS_ON_DOCKER_ENV)) ? brokerStopScript : BROKER_STOP_SCRIPT;
    }

    public static final String FE_CONF_FILE = "fe.conf";
    public static final String BE_CONF_FILE = "be.conf";
    public static final String BROKER_CONF_FILE = "apache_hdfs_broker.conf";
    public static final String BAIDU_BROKER_CONF_FILE = "baidu_star_broker.conf";

    public static final String PACKAGE_DOWNLOAD_SCRIPT = "download_star.sh";

    // TODO:Later, it will be defined uniformly through the cluster module service template
    public static final String FE_JDBC_SERVICE = "fe_jdbc";
    public static final String FE_HTTP_SERVICE = "fe_http";
    public static final String FE_EDIT_SERVICE = "fe_edit";

    public static final String BE_HEARTBEAT_SERVICE = "be_heartbeat";
    public static final String BE_HTTP_SERVICE = "be_http";

    public static final String BROKER_PRC_SERVICE = "broker_rpc";

    public static final Map<String, String> BAIDU_BROKER_CONFIG_DEDAULT;

    static {
        BAIDU_BROKER_CONFIG_DEDAULT = Maps.newHashMap();
        BAIDU_BROKER_CONFIG_DEDAULT.put("afs_filesystem_impl", "org.apache.hadoop.fs.DFileSystem");
        BAIDU_BROKER_CONFIG_DEDAULT.put("hdfs_filesystem_impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
        BAIDU_BROKER_CONFIG_DEDAULT.put("bos_filesystem_impl", "org.apache.hadoop.fs.bos.BaiduBosFileSystem");
        BAIDU_BROKER_CONFIG_DEDAULT.put("afs_agent_port", "20001");
        BAIDU_BROKER_CONFIG_DEDAULT.put("hdfs_agent_port", "20002");
        BAIDU_BROKER_CONFIG_DEDAULT.put("afs_client_auth_method", "3");
        BAIDU_BROKER_CONFIG_DEDAULT.put("hdfs_client_auth_method", "2");
        BAIDU_BROKER_CONFIG_DEDAULT.put("bos_client_auth_method", "2");
    }

}
